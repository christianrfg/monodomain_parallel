Antes de entrar em detalhes de implementação e abordagens utilizadas para paralelizar cada operação, deve-se enfatizar que foi utilizada a linguagem C++ na implementação da aplicação monodomínio sequencial e as paralelizadas, além de, ser feita a utilização a implementação MPICH2~\cite{mpich} do padrão MPI para paralelizar o algoritmo em memória distribuída. Para paralelizar o algoritmo em memória distribuída, foi utilizado a implementação OpenMP 5.0~\cite{openmp} do padrão OpenMP, sendo que esta implementação já vem nos compiladores gcc/g++ mais novos.

Reforçando o que foi apresentado no Pseudocódigo~\ref{alg:paralelizacao_ops}, a ideia por trás da paralelização é distribuir as estruturas (e.g., matrizes, vetores) entre uma quantidade determinada de processos/threads para realizar as operações mais rapidamente. Para toda operação a ser paralelizada, é necessário definir quantas partes (e.g., linhas de uma matriz ou pedaços de um vetor) cada processo/thread receberá.

%----------------------------------------------------------------------------------------
% Sub-Seção - Memória Distribuída (OpenMPI)
%----------------------------------------------------------------------------------------
\subsection{Memória Distribuída (OpenMPI)}

Primeiramente, para a abordagem utilizando memória distribuída, discute-se a separação dos vetores para realizar as operações de soma, subtração e produto interno em paralelo usando a paralelização em memória distribuída. A separação dos vetores para cada processo é determinada de acordo com o seguinte pseudocódigo:

\begin{pseudocodigo}
    \caption{Seleção de quanto cada processo irá receber de um vetor.}\label{alg:sends_displs}
    \begin{algorithmic}[1]
        \Procedure{separaVetor}{$size$, $taskRank$, $numTasks$}
            \State $vetorPartes[numTasks]$;
            \State $resto = 0$;
            \State
            \For{$i = 0,\ numTasks$}
                \State $vetorPartes[i] = size / numTasks$;
            \EndFor
            \State $resto \pluseq size$ \% $numTasks$;
            \State $vetorPartes[numTasks] \pluseq resto$;
            \State
            \State return $vetorPartes$;
        \EndProcedure
    \end{algorithmic}
\end{pseudocodigo}

O Pseudocódigo~\ref{alg:sends_displs} é bem direto e retorna um vetor especificando quantas partes do vetor cada processo receberá. Os parâmetros $size$, $taskRank$ e $numTasks$ são referentes ao tamanho do vetor, id do processo e quantidade total de processos, respectivamente. Cada processo recebe uma parte do vetor e o último processo recebe o resto da divisão (caso o vetor tenha tamanho ímpar).

A Figura~\ref{fig:soma_vetores} apresenta um exemplo da operação soma realizada em paralelo, considerando três processadores, de acordo com a separação do pseudocódigo explicado anteriormente. Cada cor do vetor na figura representa a parte que cada processo receberá, sendo que no final o resultado é retornado para o processo mestre para realizar outras operações. Observando a figura, percebe-se que cada processo recebe uma parte do vetor $X$ e $Y$ para realizar a operação de soma de vetores. A partir disso, o resultado é salvo em $Z$ que é enviado para o processo mestre. O processo mestre fica responsável por juntar as partes resultantes da operação de seus processos escravos. Com essa estratégia, é possível paralelizar as operações de soma e subtração de vetores da mesma forma. Para a operação de produto interno, é necessário depois somar os resultados obtidos por processo, já que, o produto interno entre dois vetores é um valor real que representa o comprimento de um vetor pela projeção escalar de outro.

\begin{figure}[!t]
    \centering
    \caption{Soma de dois vetores em paralelo utilizando 3 processadores.}
    \label{fig:soma_vetores}
    \includegraphics[width=.4\linewidth]{figuras/soma_vetores.png}
\end{figure}

Antes de explicar a abordagem utilizada para separar as partes de uma matriz entre processos, é necessário enfatizar que foi realizada a transformação de uma matriz em um vetor contíguo, pois, para utilizar as diretrizes do MPI para enviar e receber matrizes é necessário que a matriz esteja alocada contiguamente na memória. Em outras palavras, uma matriz de $N$ linhas e $M$ colunas é representada como um vetor com $N * M$ posições. Após a transformação de uma matriz em um vetor contíguo, é necessário determinar como acessar os elementos da matriz de acordo com a matriz tradicional (por linhas e colunas). Para isso, é utilizado a fórmula $(i * size) + j$, onde, $i$ e $j$ são, respectivamente, a linha e coluna que se deseja acessar e $size$ é a quantidade total de colunas. Com a fórmula anterior, é possível que o acesso aos elementos da matriz sejam realizados da maneira tradicional no vetor contiguo (e.g., $matriz[i][j] = matriz[i * size + j]$).

Com a definição dada acima sobre uma matriz representa como um vetor contíguo, é necessário fazer apenas algumas alterações no Pseudocódigo~\ref{alg:sends_displs} para determinar quantas partes da matriz cada processo receberá. O Pseudocódigo~\ref{alg:sends_displs_matriz} mostra as alterações realizadas para determinar quantas partes cada processo recebe do vetor contíguo que representa a matriz. A única diferença nesse pseudocódigo, é o tamanho que é necessário na divisão e cálculo do resto, sendo que, é implementada a restrição que as partes enviadas devem ser compostas de linhas inteiras. Resumidamente, o pseudocódigo permite somente que linhas da matriz sejam enviadas, utilizando para isso o resto para verificar se é possível enviar uma linha a mais para o processo atual.

\begin{pseudocodigo}
    \caption{Seleção de quanto cada processo irá receber de um vetor.}\label{alg:sends_displs_matriz}
    \begin{algorithmic}[1]
        \Procedure{separaVetor}{$size$, $taskRank$, $numTasks$}
            \State $vetorPartes[numTasks]$;
            \State $resto = 0$;
            \State
            \For{$i = 0,\ numTasks$}
                \State $vetorPartes[i] = (size * size) / numTasks$;
                \State $resto \pluseq  (size * size)\ \%\ numTasks$;
                \If{$resto \geq size$}
                    \State $vetorPartes[i] \pluseq size$;
                    \State $resto \minuseq size$;
                \EndIf
            \EndFor
            \State
            \State return $vetorPartes$;
        \EndProcedure
    \end{algorithmic}
\end{pseudocodigo}

Definido como uma matriz é alocada contiguamente na memória, e como suas linhas são enviadas para cada processo, na Figura~\ref{fig:mult_matriz_vetor} é apresentado a abordagem utilizada para realizar a operação de multiplicação de uma matriz por um vetor paralelamente. A partir dessa figura, é possível observar que cada processo recebe um conjunto de linhas da matriz $X$ e o vetor $Y$ completamente para realizar a multiplicação em paralelo. Com isso, cada processo realiza a multiplicação de sua parte pelo vetor $Y$ e retorna o resultado para o processo mestre, que fica responsável por reunir os resultados dos processos escravos. Utilizando essa abordagem é necessário que cada processo tenha o vetor $Y$ para realizar a multiplicação, sendo isto uma desvantagem dessa abordagem em relação ao consumo de memória. Entretanto, apesar do aumento de memória devido ao vetor $Y$ estar em todos os processos, essa abordagem traz vantagens em relação à simplicidade e à eficiência quanto aos cálculos realizados por processo. Assim que cada processo tiver o vetor $Y$ é fácil a realização da multiplicação e o envio do resultado para o processo mestre, não necessitando de trocas de mensagem entre os processos e sincronização em relação à multiplicação das partes.

\begin{figure}[!h]
        \centering
        \caption{Multiplicação de uma matriz por um vetor utilizando 3 processadores.}
        \label{fig:mult_matriz_vetor}
        \includegraphics[width=.7\linewidth]{figuras/multiplicacao_matriz_vetor.png}
\end{figure}

Definido como cada operação é dividida entre os processos, agora é explicado como as abordagens são implementadas na linguagem C++ utilizando a implementação MPICH2 do padrão MPI. A implementação MPICH2 possui uma variedade de funções para troca de mensagens e {\it buffers} entre processos, permitindo uma fácil implementação de memória distribuída entre computadores. Não será discutido muito a fundo sobre as questões de implementação das funções, sendo que, na documentação oficial~\footnote{https://www.mpich.org/static/docs/v3.1/www3/} do MPICH é possível ter acesso às informações de cada função utilizada aqui.

Para as operações soma e subtração entre vetores, é utilizado as funções \\ \texttt{MPI\_Scatterv} e \texttt{MPI\_Getterv}. A função \texttt{MPI\_Scatterv} permite que as partes de tamanhos distintos sejam enviadas para cada processo, como foi definido no Pseudocódigo~\ref{alg:sends_displs}. Assim, para cada processo escravo (incluindo o mestre) é necessário alocar quantos valores de cada vetor será recebido pelo mesmo. Após receber as partes dos vetores, cada processo realiza a operação normalmente e guarda o resultado que será enviado para o processo mestre. Para receber e juntar os resultados de cada processo, é utilizado a função \texttt{MPI\_Getterv}, que, consiste simplesmente em unir partes dos resultados de cada processo em somente um vetor (i.e., {\it buffer}). Para a multiplicação de uma matriz por um vetor, é utilizado as mesmas funções, sendo que, agora é necessário levar em consideração a divisão da matriz (como foi explicado no Pseudocódigo~\ref{alg:sends_displs_matriz}).

Para a operação de produto interno, foi utilizado as funções \texttt{MPI\_Scatterv} e \texttt{MPI\_Allreduce}. A função \texttt{MPI\_Scatterv} foi novamente utilizada para separar os vetores em partes para ser realizado as operações nessas partes. Para juntar os resultados, a função \texttt{MPI\_Allreduce} foi utilizada, já que, no produto interno é necessário somar os resultados individuais. Assim, a função \texttt{MPI\_Allreduce} foi utilizada junto a operação \texttt{MPI\_SUM} para somar os resultados. Foi utilizado a função \texttt{MPI\_Allreduce} pelo motivo que todo processo necessita dos resultados do produto interno. A função \texttt{MPI\_Allreduce} nada mais é do que a função \texttt{MPI\_Reduce} seguida de um \texttt{MPI\_Bcast} para enviar os resultados para todos processos.

Ainda, em algumas partes do código foi utilizada a função \texttt{MPI\_Bcast} para enviar para todos processos a atualização de um vetor ou alguma variável. Isto é necessário pois, há partes no código que todos os processos necessitam de um valor de uma variável, sendo que, na maioria das vezes, esta variável é alterada somente pelo processo mestre por questão de eficiência.

%----------------------------------------------------------------------------------------
% Sub-Seção - Memória Compartilhada (OpenMP)
%----------------------------------------------------------------------------------------
\subsection{Memória Compartilhada (OpenMP)}

Para a abordagem com memória compartilhada a ideia foi a mesma utilizada na subseção anterior. O objetivo utilizando essa abordagem era paralelizar cada um dos laços do tipo \textit{for} das operações contidas em \underline{\it conjugatedGradient}. Deixa-se em destaque que, agora é utilizado as threads disponíveis do ambiente de execução para paralelizar as operações. Vale ainda destacar que a matriz foi transformada em contígua para execução das operações.

Como os exemplos mostrados nas Figuras~\ref{fig:soma_vetores} e~\ref{fig:mult_matriz_vetor}, a paralelização utilizando memória compartilhada visa separar o trabalho para ser executado em threads separadas e o resultado ser retornado para a thread principal (mestre). O padrão OpenMP permite que o trabalho seja separado facilmente entre as threads disponíveis, sendo necessário somente algumas linhas de código em cada uma das operações. Para cada uma das operações, são definidas as estruturas que seriam compartilhadas entre as threads e como essas estruturas seriam distribuídas entre as threads disponíveis.

O Pseudocódigo~\ref{alg:paralelizacao_gen_openmp} fornece uma explicação de como cada uma das operações são paralelizadas utilizando o padrão OpenMP. A variável \textbf{estruturasCompartilhadas} no pseudocódigo define quais estruturas devem ser distribuídas entre as threads, sendo que cada thread receberá uma porção dessa estrutura. Definido quais serão as variáveis compartilhadas, é necessário fazer a inicialização das variáveis que seriam privadas para cada thread. Por fim, o \textbf{\#pragma omp for} realiza a operação em paralelo, onde, o comando \textbf{schedule(tipoSeparacao, chunk)} define como as estruturas serão enviadas para cada thread e a quantidade que será enviada para cada thread.

\begin{pseudocodigo}
    \caption{Paralelização genérica de uma operação utilizando OpenMP.}\label{alg:paralelizacao_gen_openmp}
    \begin{algorithmic}[1]
        \State \textbf{\#pragma omp parallel shared(estruturasCompartilhadas)}.
        \State Inicialização das variaveis privadas entre as threads.
        \State \textbf{\#pragma omp for schedule(tipoSeparacao, chunk)}.
        \State Operação a ser realizada por cada thread.
    \end{algorithmic}
\end{pseudocodigo}

Para definir o \textbf{tipoSeparacao} é necessário saber informações das estruturas que serão divididas, onde, têm-se como objetivo descobrir como aproveitar melhor os recursos disponíveis de acordo com o trabalho a ser realizado. Na Figura~\ref{fig:tipos_schedule} são apresentados os tipos de separação que o OpenMP suporta. A partir dessa figura, é possível reparar que cada tipo possui particularidades que dependem do contexto de utilização. A distribuição Static (estática) pode ser útil quando as operações possuem complexidade iguais, enquanto a distribuição Dynamic (dinâmica) pode ser útil quando os dados apresentam distribuições diferentes entre a estrutura. Enquanto a distribuição Guided (guiada) pode ser necessária para um problema mais complexo, onde, as funções têm complexidades diferentes de acordo com seus dados.

\begin{figure}[!h]
        \centering
        \caption{Exemplos de separação suportados pelo padrão OpenMP.}{Fonte: http://pages.tacc.utexas.edu}
        \label{fig:tipos_schedule}
        \includegraphics[width=.9\linewidth]{figuras/schedules.png}
\end{figure}

Como as operações a serem realizadas são praticamente de mesma complexidade em toda as estruturas (vetores e matrizes), foi optado por utilizar a separação estática e dinâmica. A separação estática foi utilizada para os vetores, pois, cada vetor apresenta uma distribuição de dados praticamente igual entre suas posições. Para a distribuição da matriz, foi optado pela distribuição dinâmica, já que, não se pode afirmar que os dados são distribuídos igualmente as linhas da matriz.

Com as descrições de paralelização informadas acima, foi realizado a paralelização do modelo monodomínio utilizando o padrão OpenMP.

%----------------------------------------------------------------------------------------
% Sub-Seção - Paralelização Híbrida (OpenMPI + OpenMP)
%----------------------------------------------------------------------------------------
\subsection{Paralelização Híbrida (OpenMPI + OpenMP)}

Para a versão híbrida de paralelização, foi feita uma junção das paralelizações com OpenMPI e OpenMPI informadas nas subseções anteriores. A ideia por trás dessa implementação é aproveitar tanto a quantidade de processadores quanto a quantidade de threads que cada processador possui.

Assim, foi utilizado a abordagem explicada na Subseção 5.1 combinado a estratégia definida na Subseção 5.2. Com isso, os vetores/matrizes são distribuídas entre os processos e cada processo distribui essas estruturas entre suas threads para realização das operações da função \underline{\it conjugatedGradient}. Resumidamente, o processo mestre separa as estruturas entre os processos disponíveis para realizar cada operação, sendo que, cada processo possuirá uma thread mestre que ficará responsável em distribuir o trabalho entre as threads disponíveis.

Com os passos descritos nessa seção, foi possível paralelizar, utilizando diferentes abordagens definidas, o código do modelo monodomínio. Na seção a seguir, são apresentados os resultados da execução dos códigos paralelos na carga de trabalho definida na Seção 2.