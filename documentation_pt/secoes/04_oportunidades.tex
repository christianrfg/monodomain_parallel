Na seção anterior, foi mostrado informações que possibilitam identificar as partes no código sequencial que podem ser paralelizadas. A função \underline{\em multiplyMatrixVector}, pertencente à \underline{\em conjugatedGradient}, é a responsável por grande parte do tempo de execução do algoritmo. Essa função, como seu próprio nome sugere, consiste na multiplicação de uma matriz por um vetor, sendo essa operação a mais custosa no algoritmo como um todo, já que, a matriz pode crescer de forma abrupta de acordo com a malha utilizada.

A operação de multiplicação entre uma matriz e um vetor é altamente paralelizável, sendo isto uma grande possibilidade de redução do tempo de execução da aplicação monodomínio. Além desta operação, olhando mais detalhadamente para o código da função \underline{\em conjugatedGradient} é possível observar uma série de outras operações que podem ser paralelizadas. Para realizar tal análise, no Pseudocódigo~\ref{alg:conj_gradient} é apresentado todas as operações realizadas dentro de \underline{conjugatedGradient}.

\begin{pseudocodigo}
    \caption{Função {\em conjugatedGradient}.}\label{alg:conj_gradient}
    \begin{algorithmic}[1]
        \Procedure{conjugatedGradient}{$A,\ b,\ x,\ size,\ maxIterations,\ tolerance$}
            \State $A_x = A \times x$;  \Comment{multiplyMatrixVector}
            \State $r = b - A_x$;       \Comment{subtractVectors}
            \State $p = r$;             \Comment{innerProduct}
            \State $rs_{Old} = r \cdot r$;

            \For{$k = 0,\ maxIterations$}
                \State $A_x = A \times p$;                  \Comment{multiplyMatrixVector}
                \State $alfa = rs_{Old} \ /\ (p \cdot A_x)$;\Comment{innerProduct}
                \State $A_p = p * alfa$;                    \Comment{multiplyVectorScalar}
                \State $x = A_p + x$;                       \Comment{sumVectors}
                \State $A_p = A_x * alfa$;                  \Comment{multiplyVectorScalar}
                \State $r = r - A_p$;                       \Comment{subtractVectors}
                \State $rs_{New} = r \cdot r$;              \Comment{innerProduct}
                \If{$\sqrt{rs_{New}}\ <\ tolerance$};
                    \State return $k$;
                \EndIf;
                \State $beta = rs_{New} /\ rs_{Old}$;
                \State $A_p = p * beta$;                    \Comment{innerProduct}
                \State $p = r + A_p$;                       \Comment{sumVectors}
                \State $rs_{Old} = rs_{New}$;
            \EndFor
        \EndProcedure
    \end{algorithmic}
\end{pseudocodigo}

A função \underline{\em conjugatedGradient} recebe como parâmetros uma matriz ($A$), um vetor ($b$), um vetor ($x$), o tamanho dos vetores ($size$), a quantidade máxima de iterações ($maxIterations$) e a tolerância representada pela variável ($tolerance$). A partir do pseudocódigo é possível verificar que a operação \underline{\em multiplyMatrixVector}, além de ser a operação mais custosa no quesito de complexidade, é realizada múltiplas vezes dentro de um laço. No geral, o pseudocódigo possui as seguintes operações: multiplicação de uma matriz por um vetor ($\times$), subtração de vetores ($-$), soma de vetores ($+$), multiplicação de um vetor por um escalar ($*$) e produto interno entre vetores ($\cdot$). Todas essas operações são altamente paralelizáveis, já que, são operações que podem ser realizadas em partes das estruturas para depois ser feita a junção dos resultados.

Assim, foi optado por realizar a paralelização de todas as operações que foram descritas anteriormente. A partir dessa escolha, o objetivo é produzir um código que permita reduzir o tempo de execução de acordo com a quantidade de processadores disponíveis e que permita que o algoritmo seja escalável (i.e., permita uma redução do tempo de acordo com a quantidade de processadores disponíveis). Para isso, é necessário determinar como cada uma dessas operações será realizada em paralelo de acordo com a quantidade de processadores. A seguir, é apresentado a estratégia utilizada para paralelizar cada uma das operações.

No Pseudocódigo~\ref{alg:paralelizacao_ops} é apresentado a ideia por trás da paralelização de cada operação. Foi escolhido a abordagem mestre-escravo para tratar a paralelização das operações, onde, sempre haverá um mestre que determinará a carga de trabalho de cada processo escravo, sendo que, cada processo escravo ficará responsável em realizar a operação na parte recebida e enviará o resultado para o processo mestre.

\begin{pseudocodigo}
    \caption{Paralelização genérica de uma operação.}\label{alg:paralelizacao_ops}
    \begin{algorithmic}[1]
        \State O processo/thread mestre determina quantas partes da matriz cada processo ficará responsável;
        \State Cada processo/thread, incluindo o processo/thread mestre, recebe uma parte da matriz para realizar a operação;
        \State Cada processo/thread realiza a operação;
        \State Cada processo/thread envia o resultado de sua parte para o processo mestre;
        \State O processo/thread mestre junta os resultados de cada processo/thread;
    \end{algorithmic}
\end{pseudocodigo}

\newpage

Todas as abordagens de paralelização que serão utilizadas seguem a ideia do Pseudocódigo~\ref{alg:paralelizacao_ops}, onde:

\begin{itemize}
    \item Na versão usando memória distribuída (OpenMPI) serão utilizados a quantidade de processos disponíveis para paralelizar a aplicação;
    \item Na versão usando memória compartilhada (OpenMP) as threads que serão utilizadas para a paralelização;
    \item A versão híbrida tirará o proveito tanto da quantidade de processadores quanto da quantidade de threads por núcleo do ambiente de execução.
\end{itemize}

Realizando essa abordagem em cada uma das operações, espera-se uma diminuição do tempo de execução de acordo com a quantidade de processadores/threads disponíveis. A paralelização ideal seria uma redução do tempo linearmente em relação à quantidade de processadores/threads, ou seja, utilizando 2 processadores/threads o tempo cairia pela metade, utilizando 3 processadores/threads o tempo cairia para um terço, e assim em diante. Entretanto, no cenário real isso não é alcançado devido às trocas de mensagens e sincronizações entre os processos/threads e máquinas, o que acaba consumindo tempo. Levando essas questões em consideração, nas seções seguintes são apresentadas as abordagens cada uma das abordagens de paralelização, para as operações da função \underline{\em conjugatedGradient}, utilizadas neste trabalho.