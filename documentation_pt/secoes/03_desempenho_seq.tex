Após determinar a carga de trabalho da aplicação, o algoritmo sequencial foi executado para cada malha. O objetivo dessa execução é verificar informações de uso de recursos computacionais e possíveis locais de paralelização no algoritmo. Para verificar tais informações, foi utilizado o programa {\bf GNU gprof}~\cite{gprof}, que, permite verificar o tempo gasto por função na execução de um programa. Além disso, fez-se uso do programa {\bf gprof2dot}~\cite{gprof2dot} para transformar a saída do gprof (geralmente em texto) para uma figura gráfica de fácil visualização e interpretação. Por fim, usou-se também o comando {\bf time -v}, que já vêm instalado por padrão nas distribuições do Linux, para verificar quanto tempo no total demora a aplicação e quanto de memória é consumida durante a execução do mesmo.

Para execução do algoritmo em sequencial, deixou-se os parâmetros iniciais do monodomínio nos valores padrão (fornecidos pelo professor no código), sendo que, a única diferença foi no valor de tempo final que foi ajustado para $0,3$. Esse ajuste do tempo se deu devido ao tempo necessário para a execução do algoritmo em algumas das malhas, já que o tempo de execução do algoritmo é proporcional ao valor do tempo final (i.e., quantidade de iterações). Assim, escolheu-se um tempo final menor que fosse capaz de executar a aplicação do monodomínio em um tempo viável e, ainda, permitir avaliar um ganho de desempenho no algoritmo implementado em paralelo.

Na Figura~\ref{fig:gprof_malha1} tem-se a saída gráfica do gprof para a Malha 1, onde se pode observar quantas vezes cada função foi chamada e o tempo que a mesma demorou para executar em relação ao tempo total de execução do algoritmo. Vale lembrar que estas não são todas as funções do código, o gprof2dot aplica um filtro para eliminar informações de funções que não são chamadas muitas vezes e/ou não consomem muito tempo de execução.

\begin{figure}[!t]
   \centering
   \caption{Resultado do gprof2dot para a Malha 1.}
   \label{fig:gprof_malha1}
   \includegraphics[width=\linewidth]{figuras/gprof/malha1.png}
\end{figure}

Os blocos em vermelho e laranja no gráfico são referentes às funções responsáveis por maior parte do tempo de execução do algoritmo, enquanto os blocos em azul e verde possuem uma quantidade menor de influência no tempo. Dentro de cada bloco é informado o nome da função, quanto do tempo total de execução ela é responsável considerando todas suas subfunções, a quantidade do tempo total que somente ela é responsável e a quantidade de vezes que ela é chamada. A função \underline{\it getVectorIndex} acabou aparecendo nesta análise, já que foi realizada a transformação da matriz para um vetor contíguo e esta função é responsável por determinar a posição de um elemento no vetor como se fosse uma matriz. Esta função será discutida na parte de paralelização do algoritmo, sendo o foco nesta seção às partes possíveis de paralelização.

A partir dessa figura, já se consegue verificar algumas partes do código que podem ser paralelizadas, sendo que, a maior parte das malhas segue esse mesmo esquema de informação das funções. A única diferença das informações de execução entre as malhas é a quantidade de vezes que cada função é chamada, onde, aparecem as mesmas funções em vermelho nos gráficos e o tempo de execução das mesmas segue um mesmo valor. Como os blocos azuis não têm grande influência no tempo de execução total, suas respectivas funções não são consideradas na análise a seguir.

Na Tabela~\ref{tab:infos_gprof}, têm-se a média de tempo de execução e chamadas das funções que aparecem no gráfico retornado pelo gprof2dot. Não é informado a quantidade de vezes que a função \underline{\it conjugatedGradient} é chamada, pois, ela está relacionada ao tempo final definido na execução. Em outras palavras, ela é chamada exatamente a mesma quantidade de vezes para todas as malhas.

% malha1: cg: 99.70   mv: 99.70   qtdcham: 110
% malha2: cg: 99.92   mv: 99.90   qtdcham: 153
% malha3: cg: 99.96   mv: 99.94   qtdcham: 178
% malha4: cg: 99.96   mv: 99.95   qtdcham: 110
% malha5: cg: 99.98   mv: 99.97   qtdcham: 110

\begin{table}[!htb]
   \centering
   \caption{Informações de quantidade de chamadas e quantidade de tempo em relação ao tempo total de execução para as principais funções retornadas pelo programa gprof2dot.}
   \label{tab:infos_gprof}
   \begin{tabular}{lr}
       \toprule
       \multicolumn{2}{c}{\textbf{Informações de Tempo e Chamadas }}\\
       \midrule \midrule
       Tempo Médio de {\it conjugatedGradient}                     & $82,08\ \%$  \\
      Tempo Médio {\it multiplyMatrixVector}                      & $82,01\ \%$  \\
      Quantidade Média de Chamadas de {\it multiplyMatrixVector}  & $325$  \\
       \bottomrule
   \end{tabular}
\end{table}

Na tabela anterior, pode-se observar que a função \underline{\em conjugatedGradient} representa em média $82,08\ \%$ do tempo que o programa demora para executar. Além disto, a função \underline{\em multiplyMatrixVector} é chamada pela função \underline{\em conjugatedGradient} em média $325$ vezes e representa $82,01\ \%$ do tempo de execução em relação ao tempo total! Em outras palavras, a função responsável pela maior parte do tempo de \underline{\em conjugatedGradient} é a função \underline{\em multiplyMatrixVector}. Com essas informações, já se tem conhecimento de onde pode ocorrer a paralelização do código que é discutida na próxima seção.

Além das informações do tempo de cada função separadamente, na Tabela~\ref{tab:infos_ram} são exibidos o consumo de memória RAM e o tempo total (tempo de usuário + tempo de sistema) de execução do algoritmo sequencial para cada malha utilizada. A partir dessa tabela, pode-se concluir que as afirmações de consumo destacadas na Seção 2 são verdadeiras. Ou seja, a quantidade de memória consumida, além do tempo necessário para executar o algoritmo sequencial, é inversamente proporcional à variação de discretização do espaço e é proporcional ao comprimento/largura de uma malha.


\begin{table}[!htb]
   \centering
   \caption{Quantidade de memória consumida e tempo de execução por cada malha da carga de trabalho.}
   \label{tab:infos_ram}
   \begin{tabular}{lrr}
       \toprule
       \multicolumn{1}{c}{\textbf{Malha}} & \multicolumn{1}{c}{\textbf{Consumo (RAM)}} & \multicolumn{1}{c}{\textbf{Tempo (segundos)}}\\
       \midrule \midrule
       Malha 1                     & $0,163$ GB    & $102,693$ s \\
      Malha 2                     & $2,211$ GB    & $2189,65$ s \\
       Malha 3                     & $12,801$ GB   & $10312,2$ s \\
       Malha 4                     & $4,115$ GB    & $3094,14$ s \\
       \bottomrule
   \end{tabular}
\end{table}