#  Paralelização da Solução do Problema do Mondomı́nio

Este projeto apresente três abordagens de paralelização do modelo monodominío utilizando os padrões OpenMPI e OpenMP.

## Pré Requisitos

O algoritmo é implementado em C++, sendo que foi rodado na distribuição Ubuntu Linux 64-bit. Para execução do algoritmo, é recomendado ter a versão mais atual do C++  e a última versão do [MPICH2](https://www.mpich.org/). Para o OpenMP, a versão mais atual do compilador já vêm o padrão OpenMP implementado.


## Execução

Para executar o código basta executar os seguintes comandos:

### Código Paralelizado com OpenMPI
```
cd openmpi_code
make clean
make
mpirun -N QTD_PROCESSOS bin/mono2d_release -m MALHA.txt -o PASTA_OUTPUT -f TEMPOFINAL
```

### Código Paralelizado com OpenMPI
```
cd openmp_code
make clean
make
bin/mono2d_release -n QTD_THREADS -m MALHA.txt -o PASTA_OUTPUT -f TEMPOFINAL
```

### Código Paralelizado com OpenMPI + OpenMP
```
cd hybrid_code
make clean
make
mpirun -N QTD_PROCESSOS bin/mono2d_release -n QTD_THREADS -m MALHA.txt -o PASTA_OUTPUT -f TEMPOFINAL
```

Onde:

* QTD_THREADS: quantidade de threads que serão utilizadas;
* QTD_PROCESSOS: quantidade de processos que serão utilizados;
* MALHA.txt: arquivo que contém informações da malha (variação de discretização, comprimento e largura separado por linhas);
* PASTA_OUTPUT: pasta na qual será escrita os resultados de cada iteração do algoritmo;
* TEMPOFINAL: tempo final a ser considerado.

## Contéudo das Pastas

A seguir é apresentado a descrição de cada pasta no projeto:

* documentation_pt -- Documentação realizada para o trabalho de computação paralela no período 2018/2;
* hybrid_code -- Código paralelizado com OpenMPI e o OpenMP;
* openmp_code -- Código paralelizado com o OpenMP;
* openmpi_code -- Código paralelizado com OpenMPI;
* resultados -- Resultados da execução de cada uma das abordagens, incluindo o código sequencial, com a carga de trabalho (i.e., malhas) de teste.


## Authors

* **Christian Gomes** - *Parallel work* - [chrsitianrfg](https://gitlab.com/christianrfg)
* **Rafael Schetto** -  *Initial work* - [https://github.com/rsachetto]

See also the list of [contributors](https://gitlab.com/christianrfg/monodomain_parallel/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [MPI Tutorial](http://mpitutorial.com/tutorials)
* [OpenMP](https://www.openmp.org/)
* [OpenMPI](https://www.open-mpi.org/)

