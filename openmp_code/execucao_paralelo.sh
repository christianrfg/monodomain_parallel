#!/bin/bash

MESH_PATH=carga_trabalho
OUTPUT_PATH=outputs_paralelo

N=8
MESH=1

while [ $N -le 24 ]
do
    mkdir -p "${OUTPUT_PATH}/malha${MESH}"
    mkdir -p "${OUTPUT_PATH}/malha${MESH}/${N}"
    mkdir -p "${OUTPUT_PATH}/malha${MESH}/${N}/algorithm_output"
    
    MESH_OUT=$OUTPUT_PATH/malha$MESH/${N}
    
    /usr/bin/time -v bin/mono2d_release -n $N -m $MESH_PATH/malha$MESH.txt -o $MESH_OUT/algorithm_output -f 0.3
    
    gprof -a -b bin/mono2d_release gmon.out > $MESH_OUT/report.txt
    gprof2dot -n 0.1 $MESH_OUT/report.txt > $MESH_OUT/report.dot
    dot -Tpng -o $MESH_OUT/profile.png $MESH_OUT/report.dot
    
	rm gmon.out
    ((N+=8))
done
