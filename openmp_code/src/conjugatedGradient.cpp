#include "conjugatedGradient.h"
#include <cstdio>
#include <cstdlib>
#include <omp.h>

// #define NUM_THREADS 4

/* Funcao que aloca uma matrix em um array de uma dimensao (matriz contigua) */
double *allocate_vector(int qtdLinCol) {
    double *matrix = (double *)calloc(sizeof(double *), qtdLinCol);

    return matrix;
}

void sumVectors(double *vector1, double *vector2, double *result, int size, int chunk, int num_threads) {
    int i;

    #pragma omp parallel firstprivate(i) shared(vector1, vector2, result) num_threads(num_threads)
    {
        #pragma omp for schedule(static, chunk) nowait
        for (i = 0; i < size; i++) {
            result[i] = vector1[i] + vector2[i];
        }
    }
}

void subtractVectors(double *vector1, double *vector2, double *result, int size, int chunk, int num_threads) {
    int i;

    #pragma omp parallel firstprivate(i) shared(vector1, vector2, result) num_threads(num_threads)
    {
        #pragma omp for schedule(static, chunk) nowait
        for (i = 0; i < size; i++) {
            result[i] = vector1[i] - vector2[i];
        }
    }
}

void multiplyMatrixVector(double *matrix, double *vector, double *result, int size, int chunk, int num_threads) {
    int i;
    
    #pragma omp parallel firstprivate(i) shared(matrix, vector) num_threads(num_threads)
    {
        int j;
        double aux = 0;

        #pragma omp for schedule(dynamic, chunk) nowait
        for (i = 0; i < size; i++) {
            for (j = 0; j < size; j++) {
                aux += matrix[i * size + j] * vector[j];
            }

            result[i] = aux;
            aux = 0;
        }
    }
}

double innerProduct(double *x, double *y, int size, int chunk, int num_threads) {
    double result = 0.0;
    int i;

    #pragma omp parallel firstprivate(i) shared(x, y) num_threads(num_threads)
    {
        #pragma omp for schedule(static, chunk) reduction(+:result) nowait
        for (i = 0; i < size; i++) {
            result += x[i] * y[i];
        }
    }

    return result;
}

void multiplyVectorScalar(double *vector, double scalar, double *result, int size, int chunk, int num_threads) {
    int i;
    
    #pragma omp parallel firstprivate(i) shared(vector, scalar) num_threads(num_threads)
    {
        #pragma omp for schedule(static, chunk) nowait
        for (i = 0; i < size; i++) {
            result[i] = vector[i] * scalar;
        }
    }
}

int conjugatedGradient(double *A, double *b, double *x, int size, int maxIterations, double tolerance, int num_threads) {
	int k;
	double *r, *p, *Ax, *aP;
	double alfa, beta, rs_Old, rs_New;

	r = (double*) calloc(size, sizeof(double));
    p = (double*) calloc(size, sizeof(double));
    aP = (double*) calloc(size, sizeof(double));
    Ax = (double*) calloc(size, sizeof(double));

    int chunk = size / num_threads;

    multiplyMatrixVector(A, x, Ax, size, chunk, num_threads);
    subtractVectors(b, Ax, r, size, chunk, num_threads);
    
    //p = r;
    memcpy ( p, r, size*sizeof(double));

	rs_Old = innerProduct(r, r, size, chunk, num_threads);
	
	for (k = 0; k < maxIterations; k++) {
        
        multiplyMatrixVector(A, p, Ax, size, chunk, num_threads);
        alfa = (rs_Old/(innerProduct(p, Ax, size, chunk, num_threads)));
        
        multiplyVectorScalar(p, alfa, aP, size, chunk, num_threads);
        sumVectors(x, aP, x, size, chunk, num_threads);        
        
        multiplyVectorScalar(Ax, alfa, aP, size, chunk, num_threads);
        subtractVectors(r, aP, r, size, chunk, num_threads);

		rs_New = innerProduct(r, r, size, chunk, num_threads);	

		if (sqrt(rs_New) < tolerance) {
			return k;
		}
		
		beta = rs_New/rs_Old;
        multiplyVectorScalar(p, beta, aP, size, chunk, num_threads);
		sumVectors(r, aP, p, size, chunk, num_threads);
		rs_Old = rs_New;
	}	
    
	return k-1;
}