// ============================================================================
// Name        		: monodomain2D.cpp
// Author Original  : Rafael Schetto
// Maintening  		: Christian Gomes
// Version     		: v1.0
// Copyright   		: Your copyright notice
// Description 		: Application of a Monodomain in 2D using OpenMPI
// ============================================================================

/**
 * A onda tem que percorrer uma malha de 40 micrometros em 0,05 ms na direcao x
 * e 0,09 na direcao y
 **/

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <unistd.h>

#include <omp.h>

#include "conjugatedGradient.h"
//#include "ten_tusscher_2004.h"
#include "Luo_Rudy_1991.h"

#ifdef VTK
#include "VTKWriter.h"
#endif

#define UM2_TO_CM2 0.00000001
// #define NUM_THREADS 4

#ifdef DEBUG
// debugging macros so we can pin down message provenance at a glance
#define WHERESTR "[file %s, line %d]: "
#define WHEREARG __FILE__, __LINE__
#define DEBUGPRINT2(...) fprintf(stderr, __VA_ARGS__)
#define DEBUGPRINT(_fmt, ...) DEBUGPRINT2(WHERESTR _fmt, WHEREARG, __VA_ARGS__)
#else
#define DEBUGPRINT(_fmt, ...)
#endif

using namespace std;

void read_mesh(string mesh_file, double *h, double *width, double *height) {
    FILE *mesh = fopen(mesh_file.c_str(), "r");
    fscanf(mesh, "%lf\n", h);
    fscanf(mesh, "%lf\n", width);
    fscanf(mesh, "%lf\n", height);
    fclose(mesh);
}

void print_matrix(double **matrix, int nx, int ny) {
    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            cout << matrix[i][j] << "\t";
        }
        cout << endl;
    }
}

void print_double_vector(double *v, int n) {
    for (int i = 0; i < n; ++i) {
        cout << v[i] << "\n";
    }
}

void printResult(double *v1, char *out_dir, int count, int n) {

    stringstream file_name(stringstream::in | stringstream::out);
    file_name << out_dir << "/J_t_" << setw(10) << setfill('0') << count;
    FILE *out = fopen(file_name.str().c_str(), "w");
    for (int i = 0; i < n; ++i) {
        fprintf(out, "%lf\n", v1[i]);
    }

    fclose(out);
}

void fill_vector_with_value(double *vector, double val, int size) {
    for (int i = 0; i < size; ++i) {
        vector[i] = val;
    }
}

void simple_assembly_matrix(int volumes_x, int volumes_y, double h, double alpha,
                            double *matrix, double sigma_x, double sigma_y) {

    int num_neigbours = 8;
    int i, j;
    double sigma_x1, sigma_x2, sigma_y1, sigma_y2;

    int num_elements = volumes_x * volumes_y;
    double constant;

    int *neigbours = (int *)malloc(sizeof(int) * num_neigbours);

    for (int num = 0; num < num_elements; num++) {
        sigma_x1 = (2.0 * sigma_x * sigma_x) / (sigma_x + sigma_x);
        sigma_x2 = (2.0 * sigma_x * sigma_x) / (sigma_x + sigma_x);
        sigma_y1 = (2.0 * sigma_y * sigma_y) / (sigma_y + sigma_y);
        sigma_y2 = (2.0 * sigma_y * sigma_y) / (sigma_y + sigma_y);

        i = num % volumes_x;
        j = num / volumes_y;

        for (int x = 0; x < num_neigbours; x++) {
            neigbours[x] = -1;
        }

        if (j - 1 >= 0) {
            neigbours[1] = ((j - 1) * volumes_y + i);
        } else {
            sigma_y2 = 0.0;
        }

        if (i - 1 >= 0) {
            neigbours[7] = (j * volumes_y + (i - 1));
        } else {
            sigma_x2 = 0.0;
        }

        if (i + 1 < volumes_x) {
            neigbours[3] = (j * volumes_y + (i + 1));
        } else {
            sigma_x1 = 0.0;
        }

        if (j + 1 < volumes_y) {
            neigbours[5] = ((j + 1) * volumes_y + i);
        } else {
            sigma_y1 = 0.0;
        }

        constant = (sigma_x1 + sigma_x2 + sigma_y1 + sigma_y2 + alpha);

        matrix[num * num_elements + num] = constant;

        if (neigbours[1] > -1) {
            constant = -sigma_y2;
            matrix[num * num_elements + neigbours[1]] = constant;
        }

        if (neigbours[3] > -1) {
            constant = -sigma_x1;
            matrix[num * num_elements + neigbours[3]] = constant;
        }

        if (neigbours[5] > -1) {
            constant = -sigma_y1;
            matrix[num * num_elements + neigbours[5]] = constant;
        }

        if (neigbours[7] > -1) {
            constant = -sigma_x2;
            matrix[num * num_elements + neigbours[7]] = constant;
        }
    }

    free(neigbours);
}

int main(int argc, char **argv) {
    double width, height;
	int num_volumes;
	int its, print_rate = 1;
	double h, beta, cm, alpha, t;
	double final_time = 100.0, dt = 0.01;
	double initial_v;
	double i_stim, stim_dur, stim_start;
	char *mesh_file = NULL;
	char *output_dir = NULL;
	bool saveToFile;

	/* Get the number of threads by parameter 
	 * Default = 4 threads
	 * */
	int num_threads = 4;

	/* These will define the system V * v1 = v0 */
	double *V;
	double *v1, *v0;
	int c;

	while ((c = getopt (argc, argv, "sm:n:f:t:p:o:")) != -1) {
        switch (c) {
			case 'n':
				num_threads = atoi(optarg);
				break;
            case 's':
                saveToFile = true;
                break;
            case 'm':
                mesh_file = (char*) malloc(1024*sizeof(char));
                sprintf(mesh_file, "%s", optarg) ;
                break;
            case 'f':
                final_time = atof(optarg);
                break;
            case 't':
                dt = atof(optarg);
                break;
            case 'p':
                print_rate = atoi(optarg);
                break;
            case 'o':
                output_dir = (char*) malloc(2048*sizeof(char));
                sprintf(output_dir, "%s", optarg) ;
                break;
            default:
                abort ();
        }
    }

    if(mesh_file == NULL) {
		fprintf(stderr, "-m mesh_file options is required!\n");
		exit(1);
	}

	if(saveToFile && !output_dir) {
		fprintf(stderr, "-o output_dir is required with -s option!\n");
		exit(1);
	}

	read_mesh(mesh_file, &h, &width, &height);

    beta = 0.14;// 1/cm
	cm = 1.0; // uF/cm^2

	alpha = ( ((beta * cm)/dt ) * UM2_TO_CM2 ) * pow(h,2.0);

	int volumes_x = ceil(width / h);
	int volumes_y = ceil(height / h);

	num_volumes = volumes_x * volumes_y;

	real *sv;
	sv = (real*)malloc(num_volumes*NUMBER_EQUATIONS_CELL_MODEL*sizeof(real));

	//initial conditions
	for (int i = 0; i < num_volumes; i++) {
		setIC_ode_cpu(sv, i*NUMBER_EQUATIONS_CELL_MODEL, NUMBER_EQUATIONS_CELL_MODEL);
	}

    initial_v = sv[0];

	clock_t mat_init, mat_final;

	/* Aloca matriz como um vetor contiguo */
    V = allocate_vector(num_volumes * num_volumes);

	double sigma_m_l = 1.2*0.0001;
	double sigma_m_t = 0.6226*0.0001;

	mat_init = clock();
	printf("Assembling Matrix begin\n");
	simple_assembly_matrix(volumes_x, volumes_y, h, alpha, V, sigma_m_l, sigma_m_t);
	printf("Assembling Matrix End\n");

	mat_final = clock() - mat_init;
	printf("Assembling Matrix time: %lf s\n", (double) mat_final/((double) CLOCKS_PER_SEC));

    v0 = (double*) malloc(num_volumes*sizeof(double));
	v1 = (double*) malloc(num_volumes*sizeof(double));

	fill_vector_with_value(v1, initial_v, num_volumes);


	t = 0.0;
	stim_start = 0.0;
	stim_dur = 200 * dt;
	i_stim = initial_v;

	int count = 0;

	printf("System parameters: \n");
	printf("Mesh File: %s\n", mesh_file);
	printf("Initial V: %lf\n", initial_v);
	printf("Space Discretization: %lf um\n", h);
	printf("Width = %lf um, height = %lf um \n", width, height);
	printf("Volumes X = %d, Volumes Y = %d \n", volumes_x, volumes_y);
	printf("N. Elements = %d\n", num_volumes);
	printf("time step = %lf\n", dt);
	printf("Simulation Final Time = %lf\n", final_time);
	printf("Print Rate = %d\n", print_rate);
	printf("Stimulus start = %lf\n", stim_start);
	printf("Stimulus duration = %lf\n", stim_dur);

	if (saveToFile) {
    #ifdef VTK
		printf("Saving to VTK output\n");
    #else
		printf("Saving to plain text output\n\n");
    #endif
	}
	clock_t solver_init, solver_final;

	solver_init = clock();

    #ifdef VTK
	VTKWriter *vtk = new VTKWriter();
	vtk->initializeGrid(width, height, h);
    #endif

	while (t <= final_time) {
		if(saveToFile) {
			if (count % print_rate == 0) {
				#ifdef VTK
					stringstream file_name(stringstream::in | stringstream::out);
					file_name << output_dir << "/J_t_" << setw(10) << setfill('0') << count << ".vtu";
					vtk->writeSolution(file_name.str(), v1, num_volumes);
				#else
					printResult(v1, output_dir, count, num_volumes);
				#endif
			}
			count++;
		}

		/* After the first time step, we set the new v in the celular models */
		if (t > 0.0) {
			for (int i = 0; i < num_volumes; i++) {
				sv[i * NUMBER_EQUATIONS_CELL_MODEL] = v1[i];
			}
		}

		for (int i = 0; i < num_volumes; i++) {
			int x = i % volumes_x;
			//plain wave
			bool stim = (t >= stim_start) && (t <= stim_start + stim_dur) && (x*h < 400.0);
			if (stim) {
				solve_ode_cpu(dt, sv, i * NUMBER_EQUATIONS_CELL_MODEL, NUMBER_EQUATIONS_CELL_MODEL, i_stim);
			} else {
				solve_ode_cpu(dt, sv, i * NUMBER_EQUATIONS_CELL_MODEL, NUMBER_EQUATIONS_CELL_MODEL, 0.0);
			}
		}

		double v_t;  // v_t+1
		// #pragma omp parallel num_threads(num_threads)
    	// {
		// 	#pragma omp for nowait
			for (int i = 0; i < num_volumes; i++) {
				v_t = sv[i * NUMBER_EQUATIONS_CELL_MODEL];
				v0[i] =  v_t * alpha;
			}
		// }

		/* SOLVE LINEAR SYSTEM */
		its = conjugatedGradient(V, v0, v1, num_volumes, 100, 1e-16, num_threads);

		if (count % print_rate == 0) {
			printf("t = %lf, Iterations %d\n", t, its);
		}

		t += dt;
	}

	solver_final = clock() - solver_init;
	cout << "Resolution Time: " << (double) solver_final
			/ ((double) CLOCKS_PER_SEC) << " s" << endl;


	free(sv);

	// Destroy all created objects.
	free(v0);
	free(v1);

	return 0;
}
