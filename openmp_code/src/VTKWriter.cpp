/*
 * VTKWriter.cpp
 *
 *  Created on: 19/05/2011
 *      Author: sachetto
 */

#include "VTKWriter.h"
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkCellArray.h>
#include <vtkTriangle.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkXMLUnstructuredGridWriter.h>

VTKWriter::VTKWriter() {
	ug = vtkSmartPointer<vtkUnstructuredGrid>::New();
}

VTKWriter::~VTKWriter() {

}

bool fncomp (Point* lhs, Point* rhs) {

	if (lhs->y == rhs->y) {
		return lhs->x < rhs->x;
	}
	else if (lhs->x == rhs->x) {
		return lhs->y < rhs->y;
	}
	else if (lhs->y < rhs->y) {
		return true;
	}
	else if (lhs->y > rhs->y) {
		return false;
	}

	return false;

}

bool is_in_points(Point *point, vtkPoints* points) {

	vtkIdType n = points->GetNumberOfPoints();
	if (n==0) return false;

	double *aux;

	for(vtkIdType i = 0; i < n; i++) {
		aux = points->GetPoint(i);
		if ((aux[0] == point->x) && (aux[1] == point->y) && (aux[2] == point->z)) {
			return true;
		}
	}

	return false;

}

void VTKWriter::initializeGrid(double width, double height, double h) {

	//TODO: Utilizar o MergePoints do VTK

	int volumes_x = ceil(width / h);
	int volumes_y = ceil(height / h);
	double half_h = h/2.0;

	bool(*fn_pt)(Point*,Point*) = fncomp;
	std::map<Point*,int,bool(*)(Point*,Point*)> aux_points (fn_pt);
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkCellArray> cells = vtkSmartPointer<vtkCellArray>::New();
	int point_number = 0;
	double centerx, centery;

	double  i;
	double j = 0.0;

	for (int px = 0; px < volumes_x; px++) {
		i = 0.0;
		for (int py = 0; py < volumes_y; py++) {

			Point *aux1 = new Point();
			Point *aux2 = new Point();
			Point *aux3 = new Point();
			Point *aux4 = new Point();

			centerx = i+half_h;
			centery = j+half_h;

			aux1->x = centerx-half_h;
			aux1->y = centery-half_h;
			aux1->z = 0.0;
			if (!is_in_points(aux1,points)) {
				points->InsertPoint(point_number, aux1->x, aux1->y, aux1->z);
				aux_points[aux1] =  point_number;
				point_number += 1;

			}

			aux2->x = centerx+half_h;
			aux2->y = centery-half_h;
			aux2->z = 0.0;
			if (!is_in_points(aux2,points)) {
				points->InsertPoint(point_number, aux2->x, aux2->y, aux2->z);
				aux_points[aux2] =  point_number;
				point_number += 1;
			}

			aux3->x = centerx+half_h;
			aux3->y = centery+half_h;
			aux3->z = 0.0;

			if (!is_in_points(aux3, points)) {
				points->InsertPoint(point_number, aux3->x, aux3->y, aux3->z);
				aux_points[aux3] =  point_number;
				point_number += 1;
			}

			aux4->x = centerx-half_h;
			aux4->y = centery+half_h;
			aux4->z = 0.0;

			if (!is_in_points(aux4, points)) {
				points->InsertPoint(point_number, aux4->x, aux4->y, aux4->z);
				aux_points[aux4] =  point_number;
				point_number += 1;
			}

			cells->InsertNextCell(4);
			cells->InsertCellPoint(aux_points[aux1]);
			cells->InsertCellPoint(aux_points[aux2]);
			cells->InsertCellPoint(aux_points[aux3]);
			cells->InsertCellPoint(aux_points[aux4]);
			i = i + h;
		}
		j = j + h;
	}

	ug->SetPoints(points);

	ug->SetCells(VTK_QUAD, cells);

}


void VTKWriter::writeSolution(std::string filename, double *solution, int size) {

	
	vtkSmartPointer<vtkFloatArray> values =	vtkSmartPointer<vtkFloatArray>::New();
	

	for (int i=0; i < size; i++) {
		values->InsertNextValue(solution[i]);
	}

	ug->GetCellData()->SetScalars(values);
	ug->GetPointData()->SetActiveScalars("Vm");

	vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();

	writer->SetInputData(ug);
	writer->SetFileName(filename.c_str());
	writer->Write();	

}
