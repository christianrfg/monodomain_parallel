#include <cstring>
#include <cmath>

int conjugatedGradient(double *A, double *b, double *x, int size, int maxIterations, double tolerance, int num_threads);

double *allocate_vector(int qtdLinCol);