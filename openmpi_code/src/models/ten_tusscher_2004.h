#ifndef _TEN_TUSSCHER_2004_H_
#define _TEN_TUSSCHER_2004_H_

#define NUMBER_EQUATIONS_CELL_MODEL 17

#include <cmath>

// Precision to be used for the calculations
typedef double real;

void setIC_ode_cpu(real *sv, int cellID, int NEQ);
void solve_ode_cpu(real dt, real *sv, int cellID, int NEQ, real Istim);
void solve_Rush_Larsen_cpu(real *sv, real dt, int NEQ, int cellID, real Istim);

#endif
