/*
 * VTKWriter.h
 *
 *  Created on: 19/05/2011
 *      Author: sachetto
 */

#ifndef VTKWRITER_H_
#define VTKWRITER_H_

#include <string>
#include <vtkSmartPointer.h>

class vtkUnstructuredGrid;

struct Point {
	double x, y, z;
};

class VTKWriter {
public:
	VTKWriter();
	virtual ~VTKWriter();
	void initializeGrid(double width, double height, double h);
	void writeSolution(std::string filename, double *solution, int size);
private:
	vtkSmartPointer<vtkUnstructuredGrid> ug;
};

#endif /* VTKWRITER_H_ */
