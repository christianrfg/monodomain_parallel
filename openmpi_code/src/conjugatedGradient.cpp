#include "conjugatedGradient.h"
#include <cstdio>
#include <cstdlib>
#include <mpi.h>

/* Rank do processo root */
#define MASTER 0

/* Funcao que retorna o index de uma matri em uma dimensao */
int getVectorIndex(int i, int j, int numCols) { return i * numCols + j; }

/* Funcao que aloca uma matrix em um array de uma dimensao (matriz contigua) */
double *allocate_vector(int qtdLinCol) {

    double *matrix = (double *)calloc(sizeof(double *), qtdLinCol);

    return matrix;
}

void sumVectors(double *vector1, double *vector2, double *result, int size) {
    int i;
    for (i = 0; i < size; i++) {
        result[i] = vector1[i] + vector2[i];
    }
}

void subtractVectors(double *vector1, double *vector2, double *result, int size) {
    int i;

    for (i = 0; i < size; i++) {
        result[i] = vector1[i] - vector2[i];
    }
}

void multiplyMatrixVector(double *matrix, double *vector, double *result,
                          int sendcounts, int size, int taskRank) {
    int i, j;
    double aux = 0;
    for (i = 0; i < sendcounts / size; i++) {
        for (j = 0; j < size; j++) {
            aux += matrix[getVectorIndex(i, j, size)] * vector[j];
        }

        result[i] = aux;
        aux = 0;
    }
}

double innerProduct(double *x, double *y, int size) {
    double result = 0.0;
    int i;

    for (i = 0; i < size; i++) {
        result += x[i] * y[i];
    }

    return result;
}

void multiplyVectorScalar(double *vector, double scalar, double *result, int size) {

    int i;

    for (i = 0; i < size; i++) {
        result[i] = vector[i] * scalar;
    }
}

void getSendAndDisplsScatterv(int *sendcounts, int *displs, int size,
                              int taskRank, int numTasks) {
    int tmp, tmp_rem, tmp_sendcount, sum, rem;

    sum = 0;
    rem = fmod(size * size, numTasks);

    for (int i = 0; i < numTasks; i++) {
        tmp = (size * size) / numTasks;
        tmp_rem = fmod(tmp, size);
        tmp_sendcount = tmp - tmp_rem;

        sendcounts[i] = tmp_sendcount;
        rem += tmp_rem;

        if (rem >= size) {
            sendcounts[i] += size;
            rem -= size;
        }

        displs[i] = sum;
        sum += sendcounts[i];
    }
}

void getSendAndDisplsGatterv(int *sendcounts, int *displs, int size,
                             int taskRank, int numTasks) {
    int sum;

    sum = 0;
    for (int i = 0; i < numTasks; i++) {
        sendcounts[i] = sendcounts[i] / size;

        displs[i] = sum;
        sum += sendcounts[i];
    }
}

int conjugatedGradient(double *A, double *b, double *x, int size, int maxIterations,
                       double tolerance, int taskRank, int numTasks) {

    /*______________ VARIAVEIS COMUNS A TODOS PROCESSOS ______________ */
    int k, *sendcounts, *displs;
    
    double *r, *Ax, *aP, *p;
    double alfa, beta, rs_Old, rs_New;
    double subInnerProd, globalInnerProd;  /* Variaveis para armazenar o produto interno */

    double *subA,    /* Submatriz de A */
           *subAx,   /* Subvetor de Ax */
           *subB,    /* Subvetor auxiliar 1 */
           *subR;    /* Subvetor auxiliar 2 */
    /*______________ VARIAVEIS COMUNS A TODOS PROCESSOS ______________ */


    /*______________ ALOCACAO INICIAL ______________ */
    /* Vetor de quantidade e deslocamentos de vetores */
    sendcounts = (int *)calloc(sizeof(int *), numTasks);   
    displs = (int *)calloc(sizeof(int *), numTasks);

    p = allocate_vector(size);

    if (taskRank == MASTER) {
        r = allocate_vector(size);
        aP = allocate_vector(size);
        Ax = allocate_vector(size);
    } else {
        x = allocate_vector(size); /* Aloca o vetor x para a primeira multiplicacao */
    }

    /* Processo MASTER envia o vetor x para todos os outros */
    MPI_Bcast(x, size, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
    /*______________ ALOCACAO INICIAL ______________ */


    /*______________ MULTIPLICACAO EM PARALELO ______________ */
    /*______________        Ax = A * x         ______________ */
    getSendAndDisplsScatterv(sendcounts, displs, size, taskRank, numTasks);
    subA = allocate_vector(sendcounts[taskRank]);

    MPI_Scatterv(A, sendcounts, displs, MPI_DOUBLE, subA, sendcounts[taskRank],
                 MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

    subAx = allocate_vector(sendcounts[taskRank] / size);
    multiplyMatrixVector(subA, x, subAx, sendcounts[taskRank], size, taskRank);
    getSendAndDisplsGatterv(sendcounts, displs, size, taskRank, numTasks);

    /* Entrega os resultados da multiplicacao para MASTER */
    MPI_Gatherv(subAx, sendcounts[taskRank], MPI_DOUBLE, Ax,
                sendcounts, displs, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
    /*______________ MULTIPLICACAO EM PARALELO ______________ */


    /* Libera x dos outros processos após a multiplicacao */
    if (taskRank != MASTER)
        free(x);


    /*______________ SUBTRACAO EM PARALELO ______________ */
    /*______________      r = b - Ax       ______________ */
    subB = allocate_vector(sendcounts[taskRank]);

    MPI_Scatterv(b, sendcounts, displs, MPI_DOUBLE, subB, sendcounts[taskRank],
                 MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

    subR = allocate_vector(sendcounts[taskRank]);
    subtractVectors(subB, subAx, subR, sendcounts[taskRank]);

    /* Entrega os resultados da subtracao para MASTER */
    MPI_Gatherv(subR, sendcounts[taskRank], MPI_DOUBLE, r,
                sendcounts, displs, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
    /*______________ SUBTRACAO EM PARALELO ______________ */


    if (taskRank == MASTER) {
        memcpy(p, r, size * sizeof(double));
    }
    MPI_Bcast(p, size, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);


    /*______________ PRODUTO INTERNO EM PARALELO ______________ */
    /*______________        rs_Old = r . r       ______________ */
    subInnerProd = innerProduct(subR, subR, sendcounts[taskRank]);
    /* !!! substituir para AllReduce (diponivel somente nas versoes novas do MPI) !!! */
    /* Entrega os resultados do produto interno para MASTER */
    MPI_Allreduce(&subInnerProd, &rs_Old, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    /*______________ PRODUTO INTERNO EM PARALELO ______________ */


    for (k = 0; k < maxIterations; k++) {
        /*______________ MULTIPLICACAO EM PARALELO ______________ */
        /*______________       Ax = A . p          ______________ */
        getSendAndDisplsScatterv(sendcounts, displs, size, taskRank, numTasks);
        multiplyMatrixVector(subA, p, subAx, sendcounts[taskRank], size, taskRank); 
        getSendAndDisplsGatterv(sendcounts, displs, size, taskRank, numTasks);

        /* Entrega os resultados da multiplicacao para MASTER */
        MPI_Gatherv(subAx, sendcounts[taskRank], MPI_DOUBLE, Ax,
                    sendcounts, displs, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
        /*______________ MULTIPLICACAO EM PARALELO ______________ */


        /*______________ PRODUTO INTERNO EM PARALELO ______________ */
        /*______________  globalInnerProd = (p . Ax) ______________ */
        MPI_Scatterv(p, sendcounts, displs, MPI_DOUBLE, subR, sendcounts[taskRank],
                     MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

        subInnerProd = innerProduct(subR, subAx, sendcounts[taskRank]);

        /* !!! substituir para AllReduce (diponivel somente nas versoes novas do MPI) !!! */
        /* Entrega os resultados do produto interno para MASTER */
        MPI_Allreduce(&subInnerProd, &globalInnerProd, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        // MPI_Reduce(&subInnerProd, &globalInnerProd, 1, MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD);
        // MPI_Bcast(&globalInnerProd, 1, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
        /*______________ PRODUTO INTERNO EM PARALELO ______________ */


        alfa = rs_Old / globalInnerProd;


        /*______________ MULTIPLICACAO VETOR POR ESCALAR ______________ */
        /*______________         aP = p * alfa           ______________ */
        multiplyVectorScalar(subR, alfa, subB, sendcounts[taskRank]);

        /* Entrega os resultados da multiplicacao para MASTER */
        MPI_Gatherv(subB, sendcounts[taskRank], MPI_DOUBLE, aP,
                    sendcounts, displs, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
        /*______________ MULTIPLICACAO VETOR POR ESCALAR ______________ */


        /*______________    SOMA EM PARALELO   ______________ */
        /*______________     x = sum(x + aP)   ______________ */
        MPI_Scatterv(x, sendcounts, displs, MPI_DOUBLE, subR, sendcounts[taskRank],
                     MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

        sumVectors(subR, subB, subR, sendcounts[taskRank]);

        /* Entrega os resultados da soma para MASTER */
        MPI_Gatherv(subR, sendcounts[taskRank], MPI_DOUBLE, x,
                    sendcounts, displs, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
        /*______________    SOMA EM PARALELO   ______________ */


        /*______________ MULTIPLICACAO VETOR POR ESCALAR ______________ */
        /*______________         aP = Ax * alfa          ______________ */
        multiplyVectorScalar(subAx, alfa, subB, sendcounts[taskRank]);
        
        /* Entrega os resultados da multiplicacao para MASTER */
        MPI_Gatherv(subB, sendcounts[taskRank], MPI_DOUBLE, aP,
                    sendcounts, displs, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
        /*______________ MULTIPLICACAO VETOR POR ESCALAR ______________ */


        /*______________ SUBTRACAO EM PARALELO ______________ */
        /*______________        r = r - aP     ______________ */
        MPI_Scatterv(r, sendcounts, displs, MPI_DOUBLE, subR, sendcounts[taskRank],
                     MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

        subtractVectors(subR, subB, subR, sendcounts[taskRank]);

        /* Entrega os resultados da subtracao para MASTER */
        MPI_Gatherv(subR, sendcounts[taskRank], MPI_DOUBLE, r,
                    sendcounts, displs, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
        /*______________ SUBTRACAO EM PARALELO ______________ */


        /*______________ PRODUTO INTERNO EM PARALELO ______________ */
        /*______________        rs_New = r . r       ______________ */
        subInnerProd = innerProduct(subR, subR, sendcounts[taskRank]);
        // /* !!! substituir para AllReduce (diponivel somente nas versoes novas do MPI) !!! */
        /* Entrega os resultados do produto interno para MASTER */
        MPI_Reduce(&subInnerProd, &rs_New, 1, MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD);

        /* Eh necessario que todos processos tenham o valor da variavel rs_New
         * para sairem do laco caso a tolerancia seja satisfeita */
        MPI_Bcast(&rs_New, 1, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
        /*______________ PRODUTO INTERNO EM PARALELO ______________ */

        if (sqrt(rs_New) < tolerance)
            break;

        beta = rs_New/rs_Old;
        
        /*______________ MULTIPLICACAO VETOR POR ESCALAR ______________ */
        /*______________         aP = p * beta           ______________ */
        MPI_Scatterv(p, sendcounts, displs, MPI_DOUBLE, subB, sendcounts[taskRank],
                     MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

        multiplyVectorScalar(subB, beta, subB, sendcounts[taskRank]);

        /* Entrega os resultados da multiplicacao para MASTER */
        MPI_Gatherv(subB, sendcounts[taskRank], MPI_DOUBLE, aP,
                    sendcounts, displs, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
        /*______________ MULTIPLICACAO VETOR POR ESCALAR ______________ */


        /*______________    SOMA EM PARALELO   ______________ */
        /*______________     p = sum(r + aP)   ______________ */
        MPI_Scatterv(aP, sendcounts, displs, MPI_DOUBLE, subB, sendcounts[taskRank],
                     MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

        sumVectors(subR, subB, subR, sendcounts[taskRank]);

        /* Entrega os resultados da soma para MASTER */
        MPI_Gatherv(subR, sendcounts[taskRank], MPI_DOUBLE, p,
                    sendcounts, displs, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
        /*______________    SOMA EM PARALELO   ______________ */

        rs_Old = rs_New;
        /* Eh necessario atualizar o vetor p para a nova multiplicacao pelos processos */
        MPI_Bcast(p, size, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
    }

    if (taskRank == MASTER) {
        free(subA);
        free(subAx);
        free(subB);
        free(subR);
        
        free(aP);
        free(Ax);
        free(r);

        free(sendcounts);
        free(displs);

        return k;
    } else {
        free(subA);
        free(subAx);
        free(subB);
        free(subR);
        
        free(sendcounts);
        free(displs);
        
        free(p);

        return 1;
    }

}