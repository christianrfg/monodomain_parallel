#include <cstring>
#include <cmath>

int conjugatedGradient(double *A, double *b, double *x, int size, int maxIterations, double tolerance, int taskRank, int numTasks);

int getVectorIndex(int i, int j, int numCols);

double *allocate_vector(int qtdLinCol);