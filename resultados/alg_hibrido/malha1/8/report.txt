Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 98.45      3.13     3.13       30     0.10     0.10  conjugatedGradient(double*, double*, double*, int, int, double, int, int, int)
  0.63      3.15     0.02   122880     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.63      3.17     0.02        1     0.02     0.02  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.31      3.18     0.01   122880     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  0.00      3.18     0.00   122880     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.00      3.18     0.00     4096     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00      3.18     0.00      780     0.00     0.00  multiplyVectorScalar(double*, double, double*, int, int, int)
  0.00      3.18     0.00      570     0.00     0.00  innerProduct(double*, double*, int, int, int)
  0.00      3.18     0.00      510     0.00     0.00  sumVectors(double*, double*, double*, int, int, int)
  0.00      3.18     0.00      300     0.00     0.00  subtractVectors(double*, double*, double*, int, int, int)
  0.00      3.18     0.00      300     0.00     0.00  multiplyMatrixVector(double*, double*, double*, int, int, int, int)
  0.00      3.18     0.00      300     0.00     0.00  getSendAndDisplsGatterv(int*, int*, int, int, int)
  0.00      3.18     0.00      300     0.00     0.00  getSendAndDisplsScatterv(int*, int*, int, int, int)
  0.00      3.18     0.00      241     0.00     0.00  allocate_vector(int)
  0.00      3.18     0.00       30     0.00     0.00  printResult(double*, char*, int, int)
  0.00      3.18     0.00       30     0.00     0.00  std::setw(int)
  0.00      3.18     0.00       30     0.00     0.00  std::_Setfill<char> std::setfill<char>(char)
  0.00      3.18     0.00       30     0.00     0.00  std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
  0.00      3.18     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00      3.18     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
  0.00      3.18     0.00        1     0.00     3.18  main

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.31% of 3.18 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.00    3.18       1/1           __libc_csu_init [2]
[1]    100.0    0.00    3.18       1+1       main [1]
                3.13    0.00      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
                0.00    0.03  122880/122880      solve_ode_cpu(double, double*, int, int, double) [4]
                0.02    0.00       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [6]
                0.00    0.00    4096/4096        setIC_ode_cpu(double*, int, int) [10]
                0.00    0.00      30/30          printResult(double*, char*, int, int) [19]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [24]
                0.00    0.00       1/241         allocate_vector(int) [18]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [23]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00    3.18                 __libc_csu_init [2]
                0.00    3.18       1/1           main [1]
-----------------------------------------------
                3.13    0.00      30/30          main [1]
[3]     98.4    3.13    0.00      30         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
                0.00    0.00     780/780         multiplyVectorScalar(double*, double, double*, int, int, int) [11]
                0.00    0.00     570/570         innerProduct(double*, double*, int, int, int) [12]
                0.00    0.00     510/510         sumVectors(double*, double*, double*, int, int, int) [13]
                0.00    0.00     300/300         getSendAndDisplsScatterv(int*, int*, int, int, int) [17]
                0.00    0.00     300/300         multiplyMatrixVector(double*, double*, double*, int, int, int, int) [15]
                0.00    0.00     300/300         getSendAndDisplsGatterv(int*, int*, int, int, int) [16]
                0.00    0.00     300/300         subtractVectors(double*, double*, double*, int, int, int) [14]
                0.00    0.00     240/241         allocate_vector(int) [18]
-----------------------------------------------
                0.00    0.03  122880/122880      main [1]
[4]      0.9    0.00    0.03  122880         solve_ode_cpu(double, double*, int, int, double) [4]
                0.02    0.01  122880/122880      solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
-----------------------------------------------
                0.02    0.01  122880/122880      solve_ode_cpu(double, double*, int, int, double) [4]
[5]      0.9    0.02    0.01  122880         solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
                0.01    0.00  122880/122880      RHS_Luo_Rudy_1991_cpu(double*, double*, double) [7]
-----------------------------------------------
                0.02    0.00       1/1           main [1]
[6]      0.6    0.02    0.00       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [6]
-----------------------------------------------
                0.01    0.00  122880/122880      solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
[7]      0.3    0.01    0.00  122880         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [7]
-----------------------------------------------
                0.00    0.00    4096/4096        main [1]
[10]     0.0    0.00    0.00    4096         setIC_ode_cpu(double*, int, int) [10]
-----------------------------------------------
                0.00    0.00     780/780         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[11]     0.0    0.00    0.00     780         multiplyVectorScalar(double*, double, double*, int, int, int) [11]
-----------------------------------------------
                0.00    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[12]     0.0    0.00    0.00     570         innerProduct(double*, double*, int, int, int) [12]
-----------------------------------------------
                0.00    0.00     510/510         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[13]     0.0    0.00    0.00     510         sumVectors(double*, double*, double*, int, int, int) [13]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[14]     0.0    0.00    0.00     300         subtractVectors(double*, double*, double*, int, int, int) [14]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[15]     0.0    0.00    0.00     300         multiplyMatrixVector(double*, double*, double*, int, int, int, int) [15]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[16]     0.0    0.00    0.00     300         getSendAndDisplsGatterv(int*, int*, int, int, int) [16]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[17]     0.0    0.00    0.00     300         getSendAndDisplsScatterv(int*, int*, int, int, int) [17]
-----------------------------------------------
                0.00    0.00       1/241         main [1]
                0.00    0.00     240/241         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[18]     0.0    0.00    0.00     241         allocate_vector(int) [18]
-----------------------------------------------
                0.00    0.00      30/30          main [1]
[19]     0.0    0.00    0.00      30         printResult(double*, char*, int, int) [19]
                0.00    0.00      30/30          std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [22]
                0.00    0.00      30/30          std::setw(int) [20]
                0.00    0.00      30/30          std::_Setfill<char> std::setfill<char>(char) [21]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [19]
[20]     0.0    0.00    0.00      30         std::setw(int) [20]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [19]
[21]     0.0    0.00    0.00      30         std::_Setfill<char> std::setfill<char>(char) [21]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [19]
[22]     0.0    0.00    0.00      30         std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [22]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[23]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [23]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[24]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [24]
-----------------------------------------------

Index by function name

  [13] sumVectors(double*, double*, double*, int, int, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [5] solve_Forward_Euler_cpu(double*, double, int, int, double)
  [19] printResult(double*, char*, int, int) [15] multiplyMatrixVector(double*, double*, double*, int, int, int, int) [17] getSendAndDisplsScatterv(int*, int*, int, int, int)
  [12] innerProduct(double*, double*, int, int, int) [11] multiplyVectorScalar(double*, double, double*, int, int, int) [24] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
  [10] setIC_ode_cpu(double*, int, int) [7] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [20] std::setw(int)
   [4] solve_ode_cpu(double, double*, int, int, double) [23] fill_vector_with_value(double*, double, int) [21] std::_Setfill<char> std::setfill<char>(char)
  [18] allocate_vector(int)    [6] simple_assembly_matrix(int, int, double, double, double*, double, double) [22] std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
  [14] subtractVectors(double*, double*, double*, int, int, int) [16] getSendAndDisplsGatterv(int*, int*, int, int, int) [1] main
