Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 99.30     49.81    49.81       30     1.66     1.66  conjugatedGradient(double*, double*, double*, int, int, double, int, int, int)
  0.26     49.94     0.13   675000     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  0.20     50.04     0.10        1     0.10     0.10  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.12     50.10     0.06   675000     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.10     50.15     0.05        1     0.05    50.17  main
  0.02     50.16     0.01      300     0.00     0.00  getSendAndDisplsScatterv(int*, int*, int, int, int)
  0.02     50.17     0.01       30     0.00     0.00  printResult(double*, char*, int, int)
  0.00     50.17     0.00   675000     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.00     50.17     0.00    22500     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00     50.17     0.00      780     0.00     0.00  multiplyVectorScalar(double*, double, double*, int, int, int)
  0.00     50.17     0.00      570     0.00     0.00  innerProduct(double*, double*, int, int, int)
  0.00     50.17     0.00      510     0.00     0.00  sumVectors(double*, double*, double*, int, int, int)
  0.00     50.17     0.00      300     0.00     0.00  subtractVectors(double*, double*, double*, int, int, int)
  0.00     50.17     0.00      300     0.00     0.00  multiplyMatrixVector(double*, double*, double*, int, int, int, int)
  0.00     50.17     0.00      300     0.00     0.00  getSendAndDisplsGatterv(int*, int*, int, int, int)
  0.00     50.17     0.00      241     0.00     0.00  allocate_vector(int)
  0.00     50.17     0.00       30     0.00     0.00  std::setw(int)
  0.00     50.17     0.00       30     0.00     0.00  std::_Setfill<char> std::setfill<char>(char)
  0.00     50.17     0.00       30     0.00     0.00  std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
  0.00     50.17     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00     50.17     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.02% of 50.17 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.05   50.12       1/1           __libc_csu_init [2]
[1]    100.0    0.05   50.12       1+1       main [1]
               49.81    0.01      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
                0.00    0.19  675000/675000      solve_ode_cpu(double, double*, int, int, double) [4]
                0.10    0.00       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [7]
                0.01    0.00      30/30          printResult(double*, char*, int, int) [9]
                0.00    0.00   22500/22500       setIC_ode_cpu(double*, int, int) [12]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [24]
                0.00    0.00       1/241         allocate_vector(int) [19]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [23]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00   50.17                 __libc_csu_init [2]
                0.05   50.12       1/1           main [1]
-----------------------------------------------
               49.81    0.01      30/30          main [1]
[3]     99.3   49.81    0.01      30         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
                0.01    0.00     300/300         getSendAndDisplsScatterv(int*, int*, int, int, int) [8]
                0.00    0.00     780/780         multiplyVectorScalar(double*, double, double*, int, int, int) [13]
                0.00    0.00     570/570         innerProduct(double*, double*, int, int, int) [14]
                0.00    0.00     510/510         sumVectors(double*, double*, double*, int, int, int) [15]
                0.00    0.00     300/300         multiplyMatrixVector(double*, double*, double*, int, int, int, int) [17]
                0.00    0.00     300/300         getSendAndDisplsGatterv(int*, int*, int, int, int) [18]
                0.00    0.00     300/300         subtractVectors(double*, double*, double*, int, int, int) [16]
                0.00    0.00     240/241         allocate_vector(int) [19]
-----------------------------------------------
                0.00    0.19  675000/675000      main [1]
[4]      0.4    0.00    0.19  675000         solve_ode_cpu(double, double*, int, int, double) [4]
                0.06    0.13  675000/675000      solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
-----------------------------------------------
                0.06    0.13  675000/675000      solve_ode_cpu(double, double*, int, int, double) [4]
[5]      0.4    0.06    0.13  675000         solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
                0.13    0.00  675000/675000      RHS_Luo_Rudy_1991_cpu(double*, double*, double) [6]
-----------------------------------------------
                0.13    0.00  675000/675000      solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
[6]      0.3    0.13    0.00  675000         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [6]
-----------------------------------------------
                0.10    0.00       1/1           main [1]
[7]      0.2    0.10    0.00       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [7]
-----------------------------------------------
                0.01    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[8]      0.0    0.01    0.00     300         getSendAndDisplsScatterv(int*, int*, int, int, int) [8]
-----------------------------------------------
                0.01    0.00      30/30          main [1]
[9]      0.0    0.01    0.00      30         printResult(double*, char*, int, int) [9]
                0.00    0.00      30/30          std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [22]
                0.00    0.00      30/30          std::setw(int) [20]
                0.00    0.00      30/30          std::_Setfill<char> std::setfill<char>(char) [21]
-----------------------------------------------
                0.00    0.00   22500/22500       main [1]
[12]     0.0    0.00    0.00   22500         setIC_ode_cpu(double*, int, int) [12]
-----------------------------------------------
                0.00    0.00     780/780         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[13]     0.0    0.00    0.00     780         multiplyVectorScalar(double*, double, double*, int, int, int) [13]
-----------------------------------------------
                0.00    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[14]     0.0    0.00    0.00     570         innerProduct(double*, double*, int, int, int) [14]
-----------------------------------------------
                0.00    0.00     510/510         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[15]     0.0    0.00    0.00     510         sumVectors(double*, double*, double*, int, int, int) [15]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[16]     0.0    0.00    0.00     300         subtractVectors(double*, double*, double*, int, int, int) [16]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[17]     0.0    0.00    0.00     300         multiplyMatrixVector(double*, double*, double*, int, int, int, int) [17]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[18]     0.0    0.00    0.00     300         getSendAndDisplsGatterv(int*, int*, int, int, int) [18]
-----------------------------------------------
                0.00    0.00       1/241         main [1]
                0.00    0.00     240/241         conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [3]
[19]     0.0    0.00    0.00     241         allocate_vector(int) [19]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [9]
[20]     0.0    0.00    0.00      30         std::setw(int) [20]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [9]
[21]     0.0    0.00    0.00      30         std::_Setfill<char> std::setfill<char>(char) [21]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [9]
[22]     0.0    0.00    0.00      30         std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [22]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[23]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [23]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[24]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [24]
-----------------------------------------------

Index by function name

  [15] sumVectors(double*, double*, double*, int, int, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int, int, int) [5] solve_Forward_Euler_cpu(double*, double, int, int, double)
   [9] printResult(double*, char*, int, int) [17] multiplyMatrixVector(double*, double*, double*, int, int, int, int) [8] getSendAndDisplsScatterv(int*, int*, int, int, int)
  [14] innerProduct(double*, double*, int, int, int) [13] multiplyVectorScalar(double*, double, double*, int, int, int) [24] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
  [12] setIC_ode_cpu(double*, int, int) [6] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [20] std::setw(int)
   [4] solve_ode_cpu(double, double*, int, int, double) [23] fill_vector_with_value(double*, double, int) [21] std::_Setfill<char> std::setfill<char>(char)
  [19] allocate_vector(int)    [7] simple_assembly_matrix(int, int, double, double, double*, double, double) [22] std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
  [16] subtractVectors(double*, double*, double*, int, int, int) [18] getSendAndDisplsGatterv(int*, int*, int, int, int) [1] main
