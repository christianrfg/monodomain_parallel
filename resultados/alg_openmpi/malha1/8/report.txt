Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 54.44      1.48     1.48      570     0.00     0.00  multiplyMatrixVector(double*, double*, double*, int, int, int)
 29.06      2.27     0.79 629145600     0.00     0.00  getVectorIndex(int, int, int)
 12.87      2.62     0.35                             RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  3.31      2.71     0.09      240     0.00     0.00  allocate_vector(int)
  0.37      2.72     0.01      600     0.00     0.00  subtractVectors(double*, double*, double*, int)
  0.00      2.72     0.00     1560     0.00     0.00  multiplyVectorScalar(double*, double, double*, int)
  0.00      2.72     0.00     1140     0.00     0.00  innerProduct(double*, double*, int)
  0.00      2.72     0.00     1020     0.00     0.00  sumVectors(double*, double*, double*, int)
  0.00      2.72     0.00      570     0.00     0.00  getSendAndDisplsGatterv(int*, int*, int, int, int)
  0.00      2.72     0.00      570     0.00     0.00  getSendAndDisplsScatterv(int*, int*, int, int, int)
  0.00      2.72     0.00       30     0.00     0.08  conjugatedGradient(double*, double*, double*, int, int, double, int, int)
  0.00      2.72     0.00        2     0.00     1.19  main

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.37% of 2.72 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.00    2.37       2/2           __libc_csu_init [3]
[1]     87.1    0.00    2.37       2+1       main [1]
                0.00    2.37      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
                                   1             main [1]
-----------------------------------------------
                0.00    2.37      30/30          main [1]
[2]     87.1    0.00    2.37      30         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
                1.48    0.79     570/570         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
                0.09    0.00     240/240         allocate_vector(int) [7]
                0.01    0.00     600/600         subtractVectors(double*, double*, double*, int) [8]
                0.00    0.00    1560/1560        multiplyVectorScalar(double*, double, double*, int) [12]
                0.00    0.00    1140/1140        innerProduct(double*, double*, int) [13]
                0.00    0.00    1020/1020        sumVectors(double*, double*, double*, int) [14]
                0.00    0.00     570/570         getSendAndDisplsScatterv(int*, int*, int, int, int) [16]
                0.00    0.00     570/570         getSendAndDisplsGatterv(int*, int*, int, int, int) [15]
-----------------------------------------------
                                                 <spontaneous>
[3]     87.1    0.00    2.37                 __libc_csu_init [3]
                0.00    2.37       2/2           main [1]
-----------------------------------------------
                1.48    0.79     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
[4]     83.5    1.48    0.79     570         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
                0.79    0.00 629145600/629145600     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.79    0.00 629145600/629145600     multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
[5]     29.0    0.79    0.00 629145600         getVectorIndex(int, int, int) [5]
-----------------------------------------------
                                                 <spontaneous>
[6]     12.9    0.35    0.00                 RHS_Luo_Rudy_1991_cpu(double*, double*, double) [6]
-----------------------------------------------
                0.09    0.00     240/240         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
[7]      3.3    0.09    0.00     240         allocate_vector(int) [7]
-----------------------------------------------
                0.01    0.00     600/600         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
[8]      0.4    0.01    0.00     600         subtractVectors(double*, double*, double*, int) [8]
-----------------------------------------------
                0.00    0.00    1560/1560        conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
[12]     0.0    0.00    0.00    1560         multiplyVectorScalar(double*, double, double*, int) [12]
-----------------------------------------------
                0.00    0.00    1140/1140        conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
[13]     0.0    0.00    0.00    1140         innerProduct(double*, double*, int) [13]
-----------------------------------------------
                0.00    0.00    1020/1020        conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
[14]     0.0    0.00    0.00    1020         sumVectors(double*, double*, double*, int) [14]
-----------------------------------------------
                0.00    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
[15]     0.0    0.00    0.00     570         getSendAndDisplsGatterv(int*, int*, int, int, int) [15]
-----------------------------------------------
                0.00    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [2]
[16]     0.0    0.00    0.00     570         getSendAndDisplsScatterv(int*, int*, int, int, int) [16]
-----------------------------------------------

Index by function name

  [14] sumVectors(double*, double*, double*, int) [8] subtractVectors(double*, double*, double*, int) [6] RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  [13] innerProduct(double*, double*, int) [2] conjugatedGradient(double*, double*, double*, int, int, double, int, int) [15] getSendAndDisplsGatterv(int*, int*, int, int, int)
   [5] getVectorIndex(int, int, int) [4] multiplyMatrixVector(double*, double*, double*, int, int, int) [16] getSendAndDisplsScatterv(int*, int*, int, int, int)
   [7] allocate_vector(int)   [12] multiplyVectorScalar(double*, double, double*, int) [1] main
