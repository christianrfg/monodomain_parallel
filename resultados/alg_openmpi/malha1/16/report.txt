Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 53.44      0.86     0.86      300     0.00     0.00  multiplyMatrixVector(double*, double*, double*, int, int, int)
 30.14      1.35     0.49 314593024     0.00     0.00  getVectorIndex(int, int, int)
  9.94      1.51     0.16   122880     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  4.66      1.58     0.08      241     0.00     0.00  allocate_vector(int)
  1.24      1.60     0.02        1     0.02     0.02  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.62      1.61     0.01   122880     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.00      1.61     0.00   122880     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.00      1.61     0.00     4096     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00      1.61     0.00      780     0.00     0.00  multiplyVectorScalar(double*, double, double*, int)
  0.00      1.61     0.00      570     0.00     0.00  innerProduct(double*, double*, int)
  0.00      1.61     0.00      510     0.00     0.00  sumVectors(double*, double*, double*, int)
  0.00      1.61     0.00      300     0.00     0.00  subtractVectors(double*, double*, double*, int)
  0.00      1.61     0.00      300     0.00     0.00  getSendAndDisplsGatterv(int*, int*, int, int, int)
  0.00      1.61     0.00      300     0.00     0.00  getSendAndDisplsScatterv(int*, int*, int, int, int)
  0.00      1.61     0.00       30     0.00     0.05  conjugatedGradient(double*, double*, double*, int, int, double, int, int)
  0.00      1.61     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00      1.61     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
  0.00      1.61     0.00        1     0.00     1.61  main

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.62% of 1.61 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.00    1.61       1/1           __libc_csu_init [2]
[1]    100.0    0.00    1.61       1+1       main [1]
                0.00    1.42      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
                0.00    0.17  122880/122880      solve_ode_cpu(double, double*, int, int, double) [6]
                0.02    0.00       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
                0.00    0.00       1/241         allocate_vector(int) [9]
                0.00    0.00    4096/4096        setIC_ode_cpu(double*, int, int) [14]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [21]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00    1.61                 __libc_csu_init [2]
                0.00    1.61       1/1           main [1]
-----------------------------------------------
                0.00    1.42      30/30          main [1]
[3]     88.2    0.00    1.42      30         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
                0.86    0.49     300/300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
                0.07    0.00     240/241         allocate_vector(int) [9]
                0.00    0.00     780/780         multiplyVectorScalar(double*, double, double*, int) [15]
                0.00    0.00     570/570         innerProduct(double*, double*, int) [16]
                0.00    0.00     510/510         sumVectors(double*, double*, double*, int) [17]
                0.00    0.00     300/300         getSendAndDisplsScatterv(int*, int*, int, int, int) [20]
                0.00    0.00     300/300         getSendAndDisplsGatterv(int*, int*, int, int, int) [19]
                0.00    0.00     300/300         subtractVectors(double*, double*, double*, int) [18]
-----------------------------------------------
                0.86    0.49     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[4]     83.5    0.86    0.49     300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
                0.49    0.00 314572800/314593024     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00    0.00   20224/314593024     simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
                0.49    0.00 314572800/314593024     multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
[5]     30.1    0.49    0.00 314593024         getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00    0.17  122880/122880      main [1]
[6]     10.6    0.00    0.17  122880         solve_ode_cpu(double, double*, int, int, double) [6]
                0.01    0.16  122880/122880      solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
-----------------------------------------------
                0.01    0.16  122880/122880      solve_ode_cpu(double, double*, int, int, double) [6]
[7]     10.6    0.01    0.16  122880         solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
                0.16    0.00  122880/122880      RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
                0.16    0.00  122880/122880      solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
[8]      9.9    0.16    0.00  122880         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
                0.00    0.00       1/241         main [1]
                0.07    0.00     240/241         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[9]      4.7    0.08    0.00     241         allocate_vector(int) [9]
-----------------------------------------------
                0.02    0.00       1/1           main [1]
[10]     1.2    0.02    0.00       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
                0.00    0.00   20224/314593024     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00    0.00    4096/4096        main [1]
[14]     0.0    0.00    0.00    4096         setIC_ode_cpu(double*, int, int) [14]
-----------------------------------------------
                0.00    0.00     780/780         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[15]     0.0    0.00    0.00     780         multiplyVectorScalar(double*, double, double*, int) [15]
-----------------------------------------------
                0.00    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[16]     0.0    0.00    0.00     570         innerProduct(double*, double*, int) [16]
-----------------------------------------------
                0.00    0.00     510/510         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[17]     0.0    0.00    0.00     510         sumVectors(double*, double*, double*, int) [17]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[18]     0.0    0.00    0.00     300         subtractVectors(double*, double*, double*, int) [18]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[19]     0.0    0.00    0.00     300         getSendAndDisplsGatterv(int*, int*, int, int, int) [19]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[20]     0.0    0.00    0.00     300         getSendAndDisplsScatterv(int*, int*, int, int, int) [20]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[21]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [21]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[22]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
-----------------------------------------------

Index by function name

  [17] sumVectors(double*, double*, double*, int) [18] subtractVectors(double*, double*, double*, int) [10] simple_assembly_matrix(int, int, double, double, double*, double, double)
  [16] innerProduct(double*, double*, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int, int) [19] getSendAndDisplsGatterv(int*, int*, int, int, int)
  [14] setIC_ode_cpu(double*, int, int) [4] multiplyMatrixVector(double*, double*, double*, int, int, int) [7] solve_Forward_Euler_cpu(double*, double, int, int, double)
   [6] solve_ode_cpu(double, double*, int, int, double) [15] multiplyVectorScalar(double*, double, double*, int) [20] getSendAndDisplsScatterv(int*, int*, int, int, int)
   [5] getVectorIndex(int, int, int) [8] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [22] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
   [9] allocate_vector(int)   [21] fill_vector_with_value(double*, double, int) [1] main
