Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 54.44     23.49    23.49      300     0.08     0.12  multiplyMatrixVector(double*, double*, double*, int, int, int)
 32.18     37.38    13.89 900677308     0.00     0.00  getVectorIndex(int, int, int)
  8.74     41.15     3.77   675000     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  4.31     43.01     1.86      241     0.01     0.01  allocate_vector(int)
  0.21     43.10     0.09        1     0.09     0.09  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.12     43.15     0.05   675000     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.02     43.16     0.01   675000     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.02     43.17     0.01        1     0.01    43.17  main
  0.00     43.17     0.00    22500     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00     43.17     0.00      780     0.00     0.00  multiplyVectorScalar(double*, double, double*, int)
  0.00     43.17     0.00      570     0.00     0.00  innerProduct(double*, double*, int)
  0.00     43.17     0.00      510     0.00     0.00  sumVectors(double*, double*, double*, int)
  0.00     43.17     0.00      300     0.00     0.00  subtractVectors(double*, double*, double*, int)
  0.00     43.17     0.00      300     0.00     0.00  getSendAndDisplsGatterv(int*, int*, int, int, int)
  0.00     43.17     0.00      300     0.00     0.00  getSendAndDisplsScatterv(int*, int*, int, int, int)
  0.00     43.17     0.00       30     0.00     1.31  conjugatedGradient(double*, double*, double*, int, int, double, int, int)
  0.00     43.17     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00     43.17     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.02% of 43.17 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.01   43.16       1/1           __libc_csu_init [2]
[1]    100.0    0.01   43.16       1+1       main [1]
                0.00   39.23      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
                0.01    3.82  675000/675000      solve_ode_cpu(double, double*, int, int, double) [6]
                0.09    0.00       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
                0.01    0.00       1/241         allocate_vector(int) [9]
                0.00    0.00   22500/22500       setIC_ode_cpu(double*, int, int) [14]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [21]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00   43.17                 __libc_csu_init [2]
                0.01   43.16       1/1           main [1]
-----------------------------------------------
                0.00   39.23      30/30          main [1]
[3]     90.9    0.00   39.23      30         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
               23.49   13.89     300/300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
                1.85    0.00     240/241         allocate_vector(int) [9]
                0.00    0.00     780/780         multiplyVectorScalar(double*, double, double*, int) [15]
                0.00    0.00     570/570         innerProduct(double*, double*, int) [16]
                0.00    0.00     510/510         sumVectors(double*, double*, double*, int) [17]
                0.00    0.00     300/300         getSendAndDisplsScatterv(int*, int*, int, int, int) [20]
                0.00    0.00     300/300         getSendAndDisplsGatterv(int*, int*, int, int, int) [19]
                0.00    0.00     300/300         subtractVectors(double*, double*, double*, int) [18]
-----------------------------------------------
               23.49   13.89     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[4]     86.6   23.49   13.89     300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
               13.89    0.00 900565408/900677308     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00    0.00  111900/900677308     simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
               13.89    0.00 900565408/900677308     multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
[5]     32.2   13.89    0.00 900677308         getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.01    3.82  675000/675000      main [1]
[6]      8.9    0.01    3.82  675000         solve_ode_cpu(double, double*, int, int, double) [6]
                0.05    3.77  675000/675000      solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
-----------------------------------------------
                0.05    3.77  675000/675000      solve_ode_cpu(double, double*, int, int, double) [6]
[7]      8.9    0.05    3.77  675000         solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
                3.77    0.00  675000/675000      RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
                3.77    0.00  675000/675000      solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
[8]      8.7    3.77    0.00  675000         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
                0.01    0.00       1/241         main [1]
                1.85    0.00     240/241         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[9]      4.3    1.86    0.00     241         allocate_vector(int) [9]
-----------------------------------------------
                0.09    0.00       1/1           main [1]
[10]     0.2    0.09    0.00       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
                0.00    0.00  111900/900677308     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00    0.00   22500/22500       main [1]
[14]     0.0    0.00    0.00   22500         setIC_ode_cpu(double*, int, int) [14]
-----------------------------------------------
                0.00    0.00     780/780         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[15]     0.0    0.00    0.00     780         multiplyVectorScalar(double*, double, double*, int) [15]
-----------------------------------------------
                0.00    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[16]     0.0    0.00    0.00     570         innerProduct(double*, double*, int) [16]
-----------------------------------------------
                0.00    0.00     510/510         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[17]     0.0    0.00    0.00     510         sumVectors(double*, double*, double*, int) [17]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[18]     0.0    0.00    0.00     300         subtractVectors(double*, double*, double*, int) [18]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[19]     0.0    0.00    0.00     300         getSendAndDisplsGatterv(int*, int*, int, int, int) [19]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[20]     0.0    0.00    0.00     300         getSendAndDisplsScatterv(int*, int*, int, int, int) [20]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[21]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [21]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[22]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
-----------------------------------------------

Index by function name

  [17] sumVectors(double*, double*, double*, int) [18] subtractVectors(double*, double*, double*, int) [10] simple_assembly_matrix(int, int, double, double, double*, double, double)
  [16] innerProduct(double*, double*, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int, int) [19] getSendAndDisplsGatterv(int*, int*, int, int, int)
  [14] setIC_ode_cpu(double*, int, int) [4] multiplyMatrixVector(double*, double*, double*, int, int, int) [7] solve_Forward_Euler_cpu(double*, double, int, int, double)
   [6] solve_ode_cpu(double, double*, int, int, double) [15] multiplyVectorScalar(double*, double, double*, int) [20] getSendAndDisplsScatterv(int*, int*, int, int, int)
   [5] getVectorIndex(int, int, int) [8] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [22] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
   [9] allocate_vector(int)   [21] fill_vector_with_value(double*, double, int) [1] main
