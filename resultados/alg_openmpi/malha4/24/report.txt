Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 68.86     42.21    42.21      300     0.14     0.19  multiplyMatrixVector(double*, double*, double*, int, int, int)
 21.90     55.64    13.43 2029894604     0.00     0.00  getVectorIndex(int, int, int)
  6.32     59.51     3.87   675000     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  2.66     61.14     1.63      241     0.01     0.01  allocate_vector(int)
  0.16     61.24     0.10        1     0.10     0.10  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.08     61.29     0.05   675000     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.05     61.32     0.03        1     0.03    61.33  main
  0.02     61.33     0.01      570     0.00     0.00  innerProduct(double*, double*, int)
  0.00     61.33     0.00   675000     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.00     61.33     0.00    22500     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00     61.33     0.00      780     0.00     0.00  multiplyVectorScalar(double*, double, double*, int)
  0.00     61.33     0.00      510     0.00     0.00  sumVectors(double*, double*, double*, int)
  0.00     61.33     0.00      300     0.00     0.00  subtractVectors(double*, double*, double*, int)
  0.00     61.33     0.00      300     0.00     0.00  getSendAndDisplsGatterv(int*, int*, int, int, int)
  0.00     61.33     0.00      300     0.00     0.00  getSendAndDisplsScatterv(int*, int*, int, int, int)
  0.00     61.33     0.00       30     0.00     1.91  conjugatedGradient(double*, double*, double*, int, int, double, int, int)
  0.00     61.33     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00     61.33     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.02% of 61.33 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.03   61.30       1/1           __libc_csu_init [2]
[1]    100.0    0.03   61.30       1+1       main [1]
                0.00   57.27      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
                0.00    3.92  675000/675000      solve_ode_cpu(double, double*, int, int, double) [6]
                0.10    0.00       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
                0.01    0.00       1/241         allocate_vector(int) [9]
                0.00    0.00   22500/22500       setIC_ode_cpu(double*, int, int) [15]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [21]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00   61.33                 __libc_csu_init [2]
                0.03   61.30       1/1           main [1]
-----------------------------------------------
                0.00   57.27      30/30          main [1]
[3]     93.4    0.00   57.27      30         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
               42.21   13.43     300/300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
                1.62    0.00     240/241         allocate_vector(int) [9]
                0.01    0.00     570/570         innerProduct(double*, double*, int) [11]
                0.00    0.00     780/780         multiplyVectorScalar(double*, double, double*, int) [16]
                0.00    0.00     510/510         sumVectors(double*, double*, double*, int) [17]
                0.00    0.00     300/300         getSendAndDisplsScatterv(int*, int*, int, int, int) [20]
                0.00    0.00     300/300         getSendAndDisplsGatterv(int*, int*, int, int, int) [19]
                0.00    0.00     300/300         subtractVectors(double*, double*, double*, int) [18]
-----------------------------------------------
               42.21   13.43     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[4]     90.7   42.21   13.43     300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
               13.43    0.00 2029782704/2029894604     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00    0.00  111900/2029894604     simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
               13.43    0.00 2029782704/2029894604     multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
[5]     21.9   13.43    0.00 2029894604         getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00    3.92  675000/675000      main [1]
[6]      6.4    0.00    3.92  675000         solve_ode_cpu(double, double*, int, int, double) [6]
                0.05    3.87  675000/675000      solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
-----------------------------------------------
                0.05    3.87  675000/675000      solve_ode_cpu(double, double*, int, int, double) [6]
[7]      6.4    0.05    3.87  675000         solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
                3.87    0.00  675000/675000      RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
                3.87    0.00  675000/675000      solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
[8]      6.3    3.87    0.00  675000         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
                0.01    0.00       1/241         main [1]
                1.62    0.00     240/241         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[9]      2.7    1.63    0.00     241         allocate_vector(int) [9]
-----------------------------------------------
                0.10    0.00       1/1           main [1]
[10]     0.2    0.10    0.00       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
                0.00    0.00  111900/2029894604     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.01    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[11]     0.0    0.01    0.00     570         innerProduct(double*, double*, int) [11]
-----------------------------------------------
                0.00    0.00   22500/22500       main [1]
[15]     0.0    0.00    0.00   22500         setIC_ode_cpu(double*, int, int) [15]
-----------------------------------------------
                0.00    0.00     780/780         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[16]     0.0    0.00    0.00     780         multiplyVectorScalar(double*, double, double*, int) [16]
-----------------------------------------------
                0.00    0.00     510/510         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[17]     0.0    0.00    0.00     510         sumVectors(double*, double*, double*, int) [17]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[18]     0.0    0.00    0.00     300         subtractVectors(double*, double*, double*, int) [18]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[19]     0.0    0.00    0.00     300         getSendAndDisplsGatterv(int*, int*, int, int, int) [19]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[20]     0.0    0.00    0.00     300         getSendAndDisplsScatterv(int*, int*, int, int, int) [20]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[21]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [21]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[22]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
-----------------------------------------------

Index by function name

  [17] sumVectors(double*, double*, double*, int) [18] subtractVectors(double*, double*, double*, int) [10] simple_assembly_matrix(int, int, double, double, double*, double, double)
  [11] innerProduct(double*, double*, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int, int) [19] getSendAndDisplsGatterv(int*, int*, int, int, int)
  [15] setIC_ode_cpu(double*, int, int) [4] multiplyMatrixVector(double*, double*, double*, int, int, int) [7] solve_Forward_Euler_cpu(double*, double, int, int, double)
   [6] solve_ode_cpu(double, double*, int, int, double) [16] multiplyVectorScalar(double*, double, double*, int) [20] getSendAndDisplsScatterv(int*, int*, int, int, int)
   [5] getVectorIndex(int, int, int) [8] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [22] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
   [9] allocate_vector(int)   [21] fill_vector_with_value(double*, double, int) [1] main
