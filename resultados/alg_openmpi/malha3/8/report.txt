Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 54.02    150.01   150.01      300     0.50     0.80  multiplyMatrixVector(double*, double*, double*, int, int, int)
 32.07    239.09    89.08 4165624352     0.00     0.00  getVectorIndex(int, int, int)
  9.31    264.93    25.84  1200000     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  4.52    277.49    12.55      241     0.05     0.05  allocate_vector(int)
  0.06    277.67     0.18        1     0.18     0.18  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.03    277.76     0.09  1200000     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.03    277.83     0.07        1     0.07   277.86  main
  0.01    277.85     0.02      510     0.00     0.00  sumVectors(double*, double*, double*, int)
  0.00    277.86     0.01      570     0.00     0.00  innerProduct(double*, double*, int)
  0.00    277.86     0.00  1200000     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.00    277.86     0.00    40000     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00    277.86     0.00      780     0.00     0.00  multiplyVectorScalar(double*, double, double*, int)
  0.00    277.86     0.00      300     0.00     0.00  subtractVectors(double*, double*, double*, int)
  0.00    277.86     0.00      300     0.00     0.00  getSendAndDisplsGatterv(int*, int*, int, int, int)
  0.00    277.86     0.00      300     0.00     0.00  getSendAndDisplsScatterv(int*, int*, int, int, int)
  0.00    277.86     0.00       30     0.00     8.39  conjugatedGradient(double*, double*, double*, int, int, double, int, int)
  0.00    277.86     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00    277.86     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.00% of 277.86 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.07  277.79       1/1           __libc_csu_init [2]
[1]    100.0    0.07  277.79       1+1       main [1]
                0.00  251.62      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
                0.00   25.93 1200000/1200000     solve_ode_cpu(double, double*, int, int, double) [6]
                0.18    0.00       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
                0.05    0.00       1/241         allocate_vector(int) [9]
                0.00    0.00   40000/40000       setIC_ode_cpu(double*, int, int) [16]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [21]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00  277.86                 __libc_csu_init [2]
                0.07  277.79       1/1           main [1]
-----------------------------------------------
                0.00  251.62      30/30          main [1]
[3]     90.6    0.00  251.62      30         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
              150.01   89.07     300/300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
               12.50    0.00     240/241         allocate_vector(int) [9]
                0.02    0.00     510/510         sumVectors(double*, double*, double*, int) [11]
                0.01    0.00     570/570         innerProduct(double*, double*, int) [12]
                0.00    0.00     780/780         multiplyVectorScalar(double*, double, double*, int) [17]
                0.00    0.00     300/300         getSendAndDisplsScatterv(int*, int*, int, int, int) [20]
                0.00    0.00     300/300         getSendAndDisplsGatterv(int*, int*, int, int, int) [19]
                0.00    0.00     300/300         subtractVectors(double*, double*, double*, int) [18]
-----------------------------------------------
              150.01   89.07     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[4]     86.0  150.01   89.07     300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
               89.07    0.00 4165425152/4165624352     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00    0.00  199200/4165624352     simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
               89.07    0.00 4165425152/4165624352     multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
[5]     32.1   89.08    0.00 4165624352         getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00   25.93 1200000/1200000     main [1]
[6]      9.3    0.00   25.93 1200000         solve_ode_cpu(double, double*, int, int, double) [6]
                0.09   25.84 1200000/1200000     solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
-----------------------------------------------
                0.09   25.84 1200000/1200000     solve_ode_cpu(double, double*, int, int, double) [6]
[7]      9.3    0.09   25.84 1200000         solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
               25.84    0.00 1200000/1200000     RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
               25.84    0.00 1200000/1200000     solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
[8]      9.3   25.84    0.00 1200000         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
                0.05    0.00       1/241         main [1]
               12.50    0.00     240/241         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[9]      4.5   12.55    0.00     241         allocate_vector(int) [9]
-----------------------------------------------
                0.18    0.00       1/1           main [1]
[10]     0.1    0.18    0.00       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [10]
                0.00    0.00  199200/4165624352     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.02    0.00     510/510         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[11]     0.0    0.02    0.00     510         sumVectors(double*, double*, double*, int) [11]
-----------------------------------------------
                0.01    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[12]     0.0    0.01    0.00     570         innerProduct(double*, double*, int) [12]
-----------------------------------------------
                0.00    0.00   40000/40000       main [1]
[16]     0.0    0.00    0.00   40000         setIC_ode_cpu(double*, int, int) [16]
-----------------------------------------------
                0.00    0.00     780/780         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[17]     0.0    0.00    0.00     780         multiplyVectorScalar(double*, double, double*, int) [17]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[18]     0.0    0.00    0.00     300         subtractVectors(double*, double*, double*, int) [18]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[19]     0.0    0.00    0.00     300         getSendAndDisplsGatterv(int*, int*, int, int, int) [19]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[20]     0.0    0.00    0.00     300         getSendAndDisplsScatterv(int*, int*, int, int, int) [20]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[21]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [21]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[22]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
-----------------------------------------------

Index by function name

  [11] sumVectors(double*, double*, double*, int) [18] subtractVectors(double*, double*, double*, int) [10] simple_assembly_matrix(int, int, double, double, double*, double, double)
  [12] innerProduct(double*, double*, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int, int) [19] getSendAndDisplsGatterv(int*, int*, int, int, int)
  [16] setIC_ode_cpu(double*, int, int) [4] multiplyMatrixVector(double*, double*, double*, int, int, int) [7] solve_Forward_Euler_cpu(double*, double, int, int, double)
   [6] solve_ode_cpu(double, double*, int, int, double) [17] multiplyVectorScalar(double*, double, double*, int) [20] getSendAndDisplsScatterv(int*, int*, int, int, int)
   [5] getVectorIndex(int, int, int) [8] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [22] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
   [9] allocate_vector(int)   [21] fill_vector_with_value(double*, double, int) [1] main
