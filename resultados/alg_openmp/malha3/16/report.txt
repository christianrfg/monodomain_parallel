Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 99.95    612.94   612.94       30    20.43    20.43  conjugatedGradient(double*, double*, double*, int, int, double, int)
  0.03    613.13     0.19  1200000     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  0.02    613.28     0.15        1     0.15     0.15  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.01    613.34     0.06  1200000     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.00    613.37     0.03        1     0.03   613.39  main
  0.00    613.39     0.02       30     0.00     0.00  printResult(double*, char*, int, int)
  0.00    613.39     0.00  1200000     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.00    613.39     0.00    40000     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00    613.39     0.00      780     0.00     0.00  multiplyVectorScalar(double*, double, double*, int, int, int)
  0.00    613.39     0.00      570     0.00     0.00  innerProduct(double*, double*, int, int, int)
  0.00    613.39     0.00      510     0.00     0.00  sumVectors(double*, double*, double*, int, int, int)
  0.00    613.39     0.00      300     0.00     0.00  subtractVectors(double*, double*, double*, int, int, int)
  0.00    613.39     0.00      300     0.00     0.00  multiplyMatrixVector(double*, double*, double*, int, int, int)
  0.00    613.39     0.00       30     0.00     0.00  std::setw(int)
  0.00    613.39     0.00       30     0.00     0.00  std::_Setfill<char> std::setfill<char>(char)
  0.00    613.39     0.00       30     0.00     0.00  std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
  0.00    613.39     0.00        1     0.00     0.00  allocate_vector(int)
  0.00    613.39     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00    613.39     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.00% of 613.39 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.03  613.36       1/1           __libc_csu_init [2]
[1]    100.0    0.03  613.36       1+1       main [1]
              612.94    0.00      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
                0.00    0.25 1200000/1200000     solve_ode_cpu(double, double*, int, int, double) [4]
                0.15    0.00       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [7]
                0.02    0.00      30/30          printResult(double*, char*, int, int) [8]
                0.00    0.00   40000/40000       setIC_ode_cpu(double*, int, int) [11]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
                0.00    0.00       1/1           allocate_vector(int) [20]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [21]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00  613.39                 __libc_csu_init [2]
                0.03  613.36       1/1           main [1]
-----------------------------------------------
              612.94    0.00      30/30          main [1]
[3]     99.9  612.94    0.00      30         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
                0.00    0.00     780/780         multiplyVectorScalar(double*, double, double*, int, int, int) [12]
                0.00    0.00     570/570         innerProduct(double*, double*, int, int, int) [13]
                0.00    0.00     510/510         sumVectors(double*, double*, double*, int, int, int) [14]
                0.00    0.00     300/300         multiplyMatrixVector(double*, double*, double*, int, int, int) [16]
                0.00    0.00     300/300         subtractVectors(double*, double*, double*, int, int, int) [15]
-----------------------------------------------
                0.00    0.25 1200000/1200000     main [1]
[4]      0.0    0.00    0.25 1200000         solve_ode_cpu(double, double*, int, int, double) [4]
                0.06    0.19 1200000/1200000     solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
-----------------------------------------------
                0.06    0.19 1200000/1200000     solve_ode_cpu(double, double*, int, int, double) [4]
[5]      0.0    0.06    0.19 1200000         solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
                0.19    0.00 1200000/1200000     RHS_Luo_Rudy_1991_cpu(double*, double*, double) [6]
-----------------------------------------------
                0.19    0.00 1200000/1200000     solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
[6]      0.0    0.19    0.00 1200000         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [6]
-----------------------------------------------
                0.15    0.00       1/1           main [1]
[7]      0.0    0.15    0.00       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [7]
-----------------------------------------------
                0.02    0.00      30/30          main [1]
[8]      0.0    0.02    0.00      30         printResult(double*, char*, int, int) [8]
                0.00    0.00      30/30          std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [19]
                0.00    0.00      30/30          std::setw(int) [17]
                0.00    0.00      30/30          std::_Setfill<char> std::setfill<char>(char) [18]
-----------------------------------------------
                0.00    0.00   40000/40000       main [1]
[11]     0.0    0.00    0.00   40000         setIC_ode_cpu(double*, int, int) [11]
-----------------------------------------------
                0.00    0.00     780/780         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[12]     0.0    0.00    0.00     780         multiplyVectorScalar(double*, double, double*, int, int, int) [12]
-----------------------------------------------
                0.00    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[13]     0.0    0.00    0.00     570         innerProduct(double*, double*, int, int, int) [13]
-----------------------------------------------
                0.00    0.00     510/510         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[14]     0.0    0.00    0.00     510         sumVectors(double*, double*, double*, int, int, int) [14]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[15]     0.0    0.00    0.00     300         subtractVectors(double*, double*, double*, int, int, int) [15]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[16]     0.0    0.00    0.00     300         multiplyMatrixVector(double*, double*, double*, int, int, int) [16]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [8]
[17]     0.0    0.00    0.00      30         std::setw(int) [17]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [8]
[18]     0.0    0.00    0.00      30         std::_Setfill<char> std::setfill<char>(char) [18]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [8]
[19]     0.0    0.00    0.00      30         std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [19]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[20]     0.0    0.00    0.00       1         allocate_vector(int) [20]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[21]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [21]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[22]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
-----------------------------------------------

Index by function name

  [14] sumVectors(double*, double*, double*, int, int, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int) [22] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
   [8] printResult(double*, char*, int, int) [16] multiplyMatrixVector(double*, double*, double*, int, int, int) [17] std::setw(int)
  [13] innerProduct(double*, double*, int, int, int) [12] multiplyVectorScalar(double*, double, double*, int, int, int) [18] std::_Setfill<char> std::setfill<char>(char)
  [11] setIC_ode_cpu(double*, int, int) [6] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [19] std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
   [4] solve_ode_cpu(double, double*, int, int, double) [21] fill_vector_with_value(double*, double, int) [1] main
  [20] allocate_vector(int)    [7] simple_assembly_matrix(int, int, double, double, double*, double, double)
  [15] subtractVectors(double*, double*, double*, int, int, int) [5] solve_Forward_Euler_cpu(double*, double, int, int, double)
