Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 99.16      8.04     8.04       30     0.27     0.27  conjugatedGradient(double*, double*, double*, int, int, double, int)
  0.62      8.09     0.05   122880     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  0.12      8.10     0.01        1     0.01     0.01  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.12      8.11     0.01        1     0.01     8.11  main
  0.00      8.11     0.00   122880     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.00      8.11     0.00   122880     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.00      8.11     0.00     4096     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00      8.11     0.00      780     0.00     0.00  multiplyVectorScalar(double*, double, double*, int, int, int)
  0.00      8.11     0.00      570     0.00     0.00  innerProduct(double*, double*, int, int, int)
  0.00      8.11     0.00      510     0.00     0.00  sumVectors(double*, double*, double*, int, int, int)
  0.00      8.11     0.00      300     0.00     0.00  subtractVectors(double*, double*, double*, int, int, int)
  0.00      8.11     0.00      300     0.00     0.00  multiplyMatrixVector(double*, double*, double*, int, int, int)
  0.00      8.11     0.00       30     0.00     0.00  printResult(double*, char*, int, int)
  0.00      8.11     0.00       30     0.00     0.00  std::setw(int)
  0.00      8.11     0.00       30     0.00     0.00  std::_Setfill<char> std::setfill<char>(char)
  0.00      8.11     0.00       30     0.00     0.00  std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
  0.00      8.11     0.00        1     0.00     0.00  allocate_vector(int)
  0.00      8.11     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00      8.11     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.12% of 8.11 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.01    8.10       1/1           __libc_csu_init [2]
[1]    100.0    0.01    8.10       1+1       main [1]
                8.04    0.00      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
                0.00    0.05  122880/122880      solve_ode_cpu(double, double*, int, int, double) [4]
                0.01    0.00       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [7]
                0.00    0.00    4096/4096        setIC_ode_cpu(double*, int, int) [10]
                0.00    0.00      30/30          printResult(double*, char*, int, int) [16]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
                0.00    0.00       1/1           allocate_vector(int) [20]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [21]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00    8.11                 __libc_csu_init [2]
                0.01    8.10       1/1           main [1]
-----------------------------------------------
                8.04    0.00      30/30          main [1]
[3]     99.1    8.04    0.00      30         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
                0.00    0.00     780/780         multiplyVectorScalar(double*, double, double*, int, int, int) [11]
                0.00    0.00     570/570         innerProduct(double*, double*, int, int, int) [12]
                0.00    0.00     510/510         sumVectors(double*, double*, double*, int, int, int) [13]
                0.00    0.00     300/300         multiplyMatrixVector(double*, double*, double*, int, int, int) [15]
                0.00    0.00     300/300         subtractVectors(double*, double*, double*, int, int, int) [14]
-----------------------------------------------
                0.00    0.05  122880/122880      main [1]
[4]      0.6    0.00    0.05  122880         solve_ode_cpu(double, double*, int, int, double) [4]
                0.00    0.05  122880/122880      solve_Forward_Euler_cpu(double*, double, int, int, double) [6]
-----------------------------------------------
                0.05    0.00  122880/122880      solve_Forward_Euler_cpu(double*, double, int, int, double) [6]
[5]      0.6    0.05    0.00  122880         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [5]
-----------------------------------------------
                0.00    0.05  122880/122880      solve_ode_cpu(double, double*, int, int, double) [4]
[6]      0.6    0.00    0.05  122880         solve_Forward_Euler_cpu(double*, double, int, int, double) [6]
                0.05    0.00  122880/122880      RHS_Luo_Rudy_1991_cpu(double*, double*, double) [5]
-----------------------------------------------
                0.01    0.00       1/1           main [1]
[7]      0.1    0.01    0.00       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [7]
-----------------------------------------------
                0.00    0.00    4096/4096        main [1]
[10]     0.0    0.00    0.00    4096         setIC_ode_cpu(double*, int, int) [10]
-----------------------------------------------
                0.00    0.00     780/780         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[11]     0.0    0.00    0.00     780         multiplyVectorScalar(double*, double, double*, int, int, int) [11]
-----------------------------------------------
                0.00    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[12]     0.0    0.00    0.00     570         innerProduct(double*, double*, int, int, int) [12]
-----------------------------------------------
                0.00    0.00     510/510         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[13]     0.0    0.00    0.00     510         sumVectors(double*, double*, double*, int, int, int) [13]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[14]     0.0    0.00    0.00     300         subtractVectors(double*, double*, double*, int, int, int) [14]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[15]     0.0    0.00    0.00     300         multiplyMatrixVector(double*, double*, double*, int, int, int) [15]
-----------------------------------------------
                0.00    0.00      30/30          main [1]
[16]     0.0    0.00    0.00      30         printResult(double*, char*, int, int) [16]
                0.00    0.00      30/30          std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [19]
                0.00    0.00      30/30          std::setw(int) [17]
                0.00    0.00      30/30          std::_Setfill<char> std::setfill<char>(char) [18]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [16]
[17]     0.0    0.00    0.00      30         std::setw(int) [17]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [16]
[18]     0.0    0.00    0.00      30         std::_Setfill<char> std::setfill<char>(char) [18]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [16]
[19]     0.0    0.00    0.00      30         std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [19]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[20]     0.0    0.00    0.00       1         allocate_vector(int) [20]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[21]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [21]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[22]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
-----------------------------------------------

Index by function name

  [13] sumVectors(double*, double*, double*, int, int, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int) [22] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
  [16] printResult(double*, char*, int, int) [15] multiplyMatrixVector(double*, double*, double*, int, int, int) [17] std::setw(int)
  [12] innerProduct(double*, double*, int, int, int) [11] multiplyVectorScalar(double*, double, double*, int, int, int) [18] std::_Setfill<char> std::setfill<char>(char)
  [10] setIC_ode_cpu(double*, int, int) [5] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [19] std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
   [4] solve_ode_cpu(double, double*, int, int, double) [21] fill_vector_with_value(double*, double, int) [1] main
  [20] allocate_vector(int)    [7] simple_assembly_matrix(int, int, double, double, double*, double, double)
  [14] subtractVectors(double*, double*, double*, int, int, int) [6] solve_Forward_Euler_cpu(double*, double, int, int, double)
