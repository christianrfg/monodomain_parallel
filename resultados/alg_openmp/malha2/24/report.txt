Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 99.64     90.86    90.86       30     3.03     3.03  conjugatedGradient(double*, double*, double*, int, int, double, int)
  0.24     91.08     0.22   491520     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  0.07     91.15     0.06   491520     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.05     91.20     0.05        1     0.05     0.05  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.02     91.22     0.02        1     0.02    91.22  main
  0.00     91.22     0.00   491520     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.00     91.22     0.00    16384     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00     91.22     0.00     1080     0.00     0.00  multiplyVectorScalar(double*, double, double*, int, int, int)
  0.00     91.22     0.00      770     0.00     0.00  innerProduct(double*, double*, int, int, int)
  0.00     91.22     0.00      710     0.00     0.00  sumVectors(double*, double*, double*, int, int, int)
  0.00     91.22     0.00      400     0.00     0.00  subtractVectors(double*, double*, double*, int, int, int)
  0.00     91.22     0.00      400     0.00     0.00  multiplyMatrixVector(double*, double*, double*, int, int, int)
  0.00     91.22     0.00       30     0.00     0.00  printResult(double*, char*, int, int)
  0.00     91.22     0.00       30     0.00     0.00  std::setw(int)
  0.00     91.22     0.00       30     0.00     0.00  std::_Setfill<char> std::setfill<char>(char)
  0.00     91.22     0.00       30     0.00     0.00  std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
  0.00     91.22     0.00        1     0.00     0.00  allocate_vector(int)
  0.00     91.22     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00     91.22     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.01% of 91.22 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.02   91.20       1/1           __libc_csu_init [2]
[1]    100.0    0.02   91.20       1+1       main [1]
               90.86    0.00      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
                0.00    0.28  491520/491520      solve_ode_cpu(double, double*, int, int, double) [4]
                0.05    0.00       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [7]
                0.00    0.00   16384/16384       setIC_ode_cpu(double*, int, int) [10]
                0.00    0.00      30/30          printResult(double*, char*, int, int) [16]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
                0.00    0.00       1/1           allocate_vector(int) [20]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [21]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00   91.22                 __libc_csu_init [2]
                0.02   91.20       1/1           main [1]
-----------------------------------------------
               90.86    0.00      30/30          main [1]
[3]     99.6   90.86    0.00      30         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
                0.00    0.00    1080/1080        multiplyVectorScalar(double*, double, double*, int, int, int) [11]
                0.00    0.00     770/770         innerProduct(double*, double*, int, int, int) [12]
                0.00    0.00     710/710         sumVectors(double*, double*, double*, int, int, int) [13]
                0.00    0.00     400/400         multiplyMatrixVector(double*, double*, double*, int, int, int) [15]
                0.00    0.00     400/400         subtractVectors(double*, double*, double*, int, int, int) [14]
-----------------------------------------------
                0.00    0.28  491520/491520      main [1]
[4]      0.3    0.00    0.28  491520         solve_ode_cpu(double, double*, int, int, double) [4]
                0.06    0.22  491520/491520      solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
-----------------------------------------------
                0.06    0.22  491520/491520      solve_ode_cpu(double, double*, int, int, double) [4]
[5]      0.3    0.06    0.22  491520         solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
                0.22    0.00  491520/491520      RHS_Luo_Rudy_1991_cpu(double*, double*, double) [6]
-----------------------------------------------
                0.22    0.00  491520/491520      solve_Forward_Euler_cpu(double*, double, int, int, double) [5]
[6]      0.2    0.22    0.00  491520         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [6]
-----------------------------------------------
                0.05    0.00       1/1           main [1]
[7]      0.1    0.05    0.00       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [7]
-----------------------------------------------
                0.00    0.00   16384/16384       main [1]
[10]     0.0    0.00    0.00   16384         setIC_ode_cpu(double*, int, int) [10]
-----------------------------------------------
                0.00    0.00    1080/1080        conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[11]     0.0    0.00    0.00    1080         multiplyVectorScalar(double*, double, double*, int, int, int) [11]
-----------------------------------------------
                0.00    0.00     770/770         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[12]     0.0    0.00    0.00     770         innerProduct(double*, double*, int, int, int) [12]
-----------------------------------------------
                0.00    0.00     710/710         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[13]     0.0    0.00    0.00     710         sumVectors(double*, double*, double*, int, int, int) [13]
-----------------------------------------------
                0.00    0.00     400/400         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[14]     0.0    0.00    0.00     400         subtractVectors(double*, double*, double*, int, int, int) [14]
-----------------------------------------------
                0.00    0.00     400/400         conjugatedGradient(double*, double*, double*, int, int, double, int) [3]
[15]     0.0    0.00    0.00     400         multiplyMatrixVector(double*, double*, double*, int, int, int) [15]
-----------------------------------------------
                0.00    0.00      30/30          main [1]
[16]     0.0    0.00    0.00      30         printResult(double*, char*, int, int) [16]
                0.00    0.00      30/30          std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [19]
                0.00    0.00      30/30          std::setw(int) [17]
                0.00    0.00      30/30          std::_Setfill<char> std::setfill<char>(char) [18]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [16]
[17]     0.0    0.00    0.00      30         std::setw(int) [17]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [16]
[18]     0.0    0.00    0.00      30         std::_Setfill<char> std::setfill<char>(char) [18]
-----------------------------------------------
                0.00    0.00      30/30          printResult(double*, char*, int, int) [16]
[19]     0.0    0.00    0.00      30         std::operator|(std::_Ios_Openmode, std::_Ios_Openmode) [19]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[20]     0.0    0.00    0.00       1         allocate_vector(int) [20]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[21]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [21]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[22]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [22]
-----------------------------------------------

Index by function name

  [13] sumVectors(double*, double*, double*, int, int, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int) [22] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
  [16] printResult(double*, char*, int, int) [15] multiplyMatrixVector(double*, double*, double*, int, int, int) [17] std::setw(int)
  [12] innerProduct(double*, double*, int, int, int) [11] multiplyVectorScalar(double*, double, double*, int, int, int) [18] std::_Setfill<char> std::setfill<char>(char)
  [10] setIC_ode_cpu(double*, int, int) [6] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [19] std::operator|(std::_Ios_Openmode, std::_Ios_Openmode)
   [4] solve_ode_cpu(double, double*, int, int, double) [21] fill_vector_with_value(double*, double, int) [1] main
  [20] allocate_vector(int)    [7] simple_assembly_matrix(int, int, double, double, double*, double, double)
  [14] subtractVectors(double*, double*, double*, int, int, int) [5] solve_Forward_Euler_cpu(double*, double, int, int, double)
