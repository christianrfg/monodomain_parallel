Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 54.75    361.95   361.95      300     1.21     2.01  multiplyMatrixVector(double*, double*, double*, int, int, int)
 36.47    603.05   241.10 1551256540     0.00     0.00  getVectorIndex(int, int, int)
  8.75    660.92    57.87   675000     0.00     0.00  RHS_Luo_Rudy_1991_cpu(double*, double*, double)
  0.01    661.00     0.08        1     0.08     0.10  simple_assembly_matrix(int, int, double, double, double*, double, double)
  0.01    661.05     0.05   675000     0.00     0.00  solve_Forward_Euler_cpu(double*, double, int, int, double)
  0.01    661.09     0.04      570     0.00     0.00  innerProduct(double*, double*, int)
  0.01    661.13     0.04      300     0.00     0.00  subtractVectors(double*, double*, double*, int)
  0.01    661.17     0.04        1     0.04   661.21  main
  0.00    661.20     0.03      780     0.00     0.00  multiplyVectorScalar(double*, double, double*, int)
  0.00    661.21     0.01      510     0.00     0.00  sumVectors(double*, double*, double*, int)
  0.00    661.21     0.00   675000     0.00     0.00  solve_ode_cpu(double, double*, int, int, double)
  0.00    661.21     0.00    22500     0.00     0.00  setIC_ode_cpu(double*, int, int)
  0.00    661.21     0.00      300     0.00     0.00  getSendAndDisplsGatterv(int*, int*, int, int, int)
  0.00    661.21     0.00      300     0.00     0.00  getSendAndDisplsScatterv(int*, int*, int, int, int)
  0.00    661.21     0.00      241     0.00     0.00  allocate_vector(int)
  0.00    661.21     0.00       30     0.00    20.10  conjugatedGradient(double*, double*, double*, int, int, double, int, int)
  0.00    661.21     0.00        1     0.00     0.00  fill_vector_with_value(double*, double, int)
  0.00    661.21     0.00        1     0.00     0.00  read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.00% of 661.21 seconds

index % time    self  children    called     name
                                   1             main [1]
                0.04  661.17       1/1           __libc_csu_init [2]
[1]    100.0    0.04  661.17       1+1       main [1]
                0.00  603.15      30/30          conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
                0.00   57.92  675000/675000      solve_ode_cpu(double, double*, int, int, double) [6]
                0.08    0.02       1/1           simple_assembly_matrix(int, int, double, double, double*, double, double) [9]
                0.00    0.00   22500/22500       setIC_ode_cpu(double*, int, int) [16]
                0.00    0.00       1/1           read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [21]
                0.00    0.00       1/241         allocate_vector(int) [19]
                0.00    0.00       1/1           fill_vector_with_value(double*, double, int) [20]
                                   1             main [1]
-----------------------------------------------
                                                 <spontaneous>
[2]    100.0    0.00  661.21                 __libc_csu_init [2]
                0.04  661.17       1/1           main [1]
-----------------------------------------------
                0.00  603.15      30/30          main [1]
[3]     91.2    0.00  603.15      30         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
              361.95  241.08     300/300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
                0.04    0.00     570/570         innerProduct(double*, double*, int) [10]
                0.04    0.00     300/300         subtractVectors(double*, double*, double*, int) [11]
                0.03    0.00     780/780         multiplyVectorScalar(double*, double, double*, int) [12]
                0.01    0.00     510/510         sumVectors(double*, double*, double*, int) [13]
                0.00    0.00     300/300         getSendAndDisplsScatterv(int*, int*, int, int, int) [18]
                0.00    0.00     300/300         getSendAndDisplsGatterv(int*, int*, int, int, int) [17]
                0.00    0.00     240/241         allocate_vector(int) [19]
-----------------------------------------------
              361.95  241.08     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[4]     91.2  361.95  241.08     300         multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
              241.08    0.00 1551144640/1551256540     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.02    0.00  111900/1551256540     simple_assembly_matrix(int, int, double, double, double*, double, double) [9]
              241.08    0.00 1551144640/1551256540     multiplyMatrixVector(double*, double*, double*, int, int, int) [4]
[5]     36.5  241.10    0.00 1551256540         getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.00   57.92  675000/675000      main [1]
[6]      8.8    0.00   57.92  675000         solve_ode_cpu(double, double*, int, int, double) [6]
                0.05   57.87  675000/675000      solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
-----------------------------------------------
                0.05   57.87  675000/675000      solve_ode_cpu(double, double*, int, int, double) [6]
[7]      8.8    0.05   57.87  675000         solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
               57.87    0.00  675000/675000      RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
               57.87    0.00  675000/675000      solve_Forward_Euler_cpu(double*, double, int, int, double) [7]
[8]      8.8   57.87    0.00  675000         RHS_Luo_Rudy_1991_cpu(double*, double*, double) [8]
-----------------------------------------------
                0.08    0.02       1/1           main [1]
[9]      0.0    0.08    0.02       1         simple_assembly_matrix(int, int, double, double, double*, double, double) [9]
                0.02    0.00  111900/1551256540     getVectorIndex(int, int, int) [5]
-----------------------------------------------
                0.04    0.00     570/570         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[10]     0.0    0.04    0.00     570         innerProduct(double*, double*, int) [10]
-----------------------------------------------
                0.04    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[11]     0.0    0.04    0.00     300         subtractVectors(double*, double*, double*, int) [11]
-----------------------------------------------
                0.03    0.00     780/780         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[12]     0.0    0.03    0.00     780         multiplyVectorScalar(double*, double, double*, int) [12]
-----------------------------------------------
                0.01    0.00     510/510         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[13]     0.0    0.01    0.00     510         sumVectors(double*, double*, double*, int) [13]
-----------------------------------------------
                0.00    0.00   22500/22500       main [1]
[16]     0.0    0.00    0.00   22500         setIC_ode_cpu(double*, int, int) [16]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[17]     0.0    0.00    0.00     300         getSendAndDisplsGatterv(int*, int*, int, int, int) [17]
-----------------------------------------------
                0.00    0.00     300/300         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[18]     0.0    0.00    0.00     300         getSendAndDisplsScatterv(int*, int*, int, int, int) [18]
-----------------------------------------------
                0.00    0.00       1/241         main [1]
                0.00    0.00     240/241         conjugatedGradient(double*, double*, double*, int, int, double, int, int) [3]
[19]     0.0    0.00    0.00     241         allocate_vector(int) [19]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[20]     0.0    0.00    0.00       1         fill_vector_with_value(double*, double, int) [20]
-----------------------------------------------
                0.00    0.00       1/1           main [1]
[21]     0.0    0.00    0.00       1         read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*) [21]
-----------------------------------------------

Index by function name

  [13] sumVectors(double*, double*, double*, int) [11] subtractVectors(double*, double*, double*, int) [9] simple_assembly_matrix(int, int, double, double, double*, double, double)
  [10] innerProduct(double*, double*, int) [3] conjugatedGradient(double*, double*, double*, int, int, double, int, int) [17] getSendAndDisplsGatterv(int*, int*, int, int, int)
  [16] setIC_ode_cpu(double*, int, int) [4] multiplyMatrixVector(double*, double*, double*, int, int, int) [7] solve_Forward_Euler_cpu(double*, double, int, int, double)
   [6] solve_ode_cpu(double, double*, int, int, double) [12] multiplyVectorScalar(double*, double, double*, int) [18] getSendAndDisplsScatterv(int*, int*, int, int, int)
   [5] getVectorIndex(int, int, int) [8] RHS_Luo_Rudy_1991_cpu(double*, double*, double) [21] read_mesh(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, double*, double*, double*)
  [19] allocate_vector(int)   [20] fill_vector_with_value(double*, double, int) [1] main
